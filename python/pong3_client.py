#!/usr/bin/python3

from turtle import *
import _tkinter
import time
from lib.helper_funcs import *
from lib.client_socket import ClientSocket

# this is meant to give a consistant frame rate
TIME_DELAY = 0.020

WIN_WIDTH = 800
WIN_HEIGHT = 600

def draw_background(scores, time_str):
    clear()

    # # Draw a background
    pencolor('white')
    # fillcolor(None)
    penup()
    goto(-WIN_WIDTH/2, -WIN_HEIGHT/2)
    pendown()
    goto(WIN_WIDTH/2, -WIN_HEIGHT/2+1, )
    penup()
    goto(-WIN_WIDTH/2, WIN_HEIGHT/2-1)
    pendown()
    goto(WIN_WIDTH/2, WIN_HEIGHT/2-1)
    penup()
    home()

    # Draw the strings at the top
    penup()
    fillcolor('white')
    pencolor('white')
    home()
    goto(-WIN_WIDTH/2, WIN_HEIGHT/2)
    write(scores[0], font = ('Times New Roman', 36, 'bold'))

    penup()
    home()
    goto(-50, WIN_HEIGHT/2)
    write(time_str, font = ('Times New Roman', 36, 'bold'))

    penup()
    home()
    goto(WIN_WIDTH/2-50, WIN_HEIGHT/2)
    write(scores[1], font = ('Times New Roman', 36, 'bold'))

    penup()
    home()


setup_graphics(WIN_WIDTH, WIN_HEIGHT+50*2)

screen = Screen()
screen.bgcolor('black')
screen.title("Pong for CodeDojo")

PADDLE_WIDTH = 10
PADDLE_HEIGHT = 40

BALL_WIDTH = 6

PADDLE_X = WIN_WIDTH / 2 - 50
PADDLE_MAX_Y = WIN_HEIGHT / 2 - PADDLE_HEIGHT
MAX_Y = WIN_HEIGHT/2 - BALL_WIDTH

PADDLE_BUFFER = PADDLE_HEIGHT + 10

PLAYER_LEFT = 0
PLAYER_RIGHT = 1

# background = build_object(poly, 'darkblue', 'white')

poly = ((-PADDLE_WIDTH, -PADDLE_HEIGHT), \
        (-PADDLE_WIDTH, PADDLE_HEIGHT), \
        (PADDLE_WIDTH, PADDLE_HEIGHT), \
        (PADDLE_WIDTH, -PADDLE_HEIGHT))

# Create a blocker on the left side
paddles = [None, None]
paddles[PLAYER_LEFT] = build_object(poly, 'gray', 'white')
paddles[PLAYER_LEFT].setpos(-PADDLE_X, 0)

# create a blocker on the right side
paddles[PLAYER_RIGHT] = build_object(poly, 'gray', 'white')
paddles[PLAYER_RIGHT].setpos(PADDLE_X, 0)

# Make the ball shape
s = Shape("compound", None)
begin_poly()
circle(BALL_WIDTH)
end_poly()
ball_shape = get_poly()
s.addcomponent(ball_shape, "orange")
screen.addshape("ball", s)

# create the ball
ball = Object("ball")
ball.setpos(0, 0)
# setup the ball's initial movement vector
ball_speed = Vec2D(-1, -1)

key_poller = KeyPoller(screen)

is_running = True
def quit():
    global is_running
    is_running = False

def goup1():
    pos = paddles[PLAYER_LEFT].pos() + (0, 2)
    paddles[PLAYER_LEFT].setpos(pos)

def godown1():
    pos = paddles[PLAYER_LEFT].pos() + (0, -2)
    paddles[PLAYER_LEFT].setpos(pos)

scores = [0, 0]
start_time = 0
previous_time = 0
curr_time = 0

def win(player):
    global ball, ball_speed, previous_time, start_time, scores, paddles
    ball.setpos(0, 0)
    if(player == PLAYER_LEFT):
        print("Left Player wins!")
        vec = Vec2D(-1, -1)
    else:
        print("Right Player wins!")
        vec = Vec2D(1, -1)

    for p in paddles:
        p.setpos(p.x(), 0)

    scores[player] += 1
    update_time(True)

def update_time(reset):
    global previous_time, start_time, curr_time
    time_str = "00:00"
    update = False
    if reset:
        elapsed_time = start_time = time.time()
        update = True
    else:
        curr_time = time.time()
        if curr_time - elapsed_time > 1:
            diff = int(curr_time - start_time)
            mins = int(diff / 60)
            secs = diff % 60
            time_str = '{:02d}:{:02d}'.format(mins, secs)
            update = True
            elapsed_time = curr_time

    if update:
        global scores
        score_strs = [str(s) for s in scores]
        draw_background(score_strs, time_str)

update_time(True)

fps = FPS("FPS")

socket = ClientSocket("localhost", 9009)

socket.send_message("login")

while(not socket.has_message()):
    time.sleep(0.02)

msg = socket.get_message()
print(msg)

try:
    while False: # (is_running):

        prev_time = time.time()

        # get any messages
        while(socket.has_message()):
            msg = socket.get_message()

        # check the down keys
        for (keycode, key) in key_poller.keys().items():
            if(key == 'q'):
                goup1()
            elif key == 'a':
                godown1()
            elif key == 'p':
                goup2()
            elif key == 'l':
                godown2()
            elif keycode == 27: # Escape keycode
                quit()

        # Update the physics
        ball.setpos(ball.pos() + ball_speed)

        # check limits
        # First the y limits on the paddles
        if paddles[PLAYER_LEFT].y() < -PADDLE_MAX_Y:
            paddles[PLAYER_LEFT].setpos(paddles[PLAYER_LEFT].x(), -PADDLE_MAX_Y)
        elif paddles[PLAYER_LEFT].y() > PADDLE_MAX_Y:
            paddles[PLAYER_LEFT].setpos(paddles[PLAYER_LEFT].x(), PADDLE_MAX_Y)

        if paddles[PLAYER_RIGHT].y() < -PADDLE_MAX_Y:
            paddles[PLAYER_RIGHT].setpos(paddles[PLAYER_RIGHT].x(), -PADDLE_MAX_Y)
        elif paddles[PLAYER_RIGHT].y() > PADDLE_MAX_Y:
            paddles[PLAYER_RIGHT].setpos(paddles[PLAYER_RIGHT].x(), PADDLE_MAX_Y)

        # Then on the ball
        if(ball.y() <= -MAX_Y or MAX_Y <= ball.y()):
            ball_speed = Vec2D(ball_speed[0], ball_speed[1] * -1)

        if(ball.x() <= -PADDLE_X):
            #Check if the ball hits the paddle
            if (paddles[PLAYER_LEFT].y()-PADDLE_BUFFER <= ball.y() and \
                ball.y() <= paddles[PLAYER_LEFT].y()+PADDLE_BUFFER ):
                ball_speed = Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                win(PLAYER_RIGHT)
        elif(ball.x() >= PADDLE_X):
            #Check if the ball hits the paddle
            if (paddles[PLAYER_RIGHT].y()-PADDLE_BUFFER <= ball.y() and \
                ball.y() <= paddles[PLAYER_RIGHT].y()+PADDLE_BUFFER ):
                ball_speed = Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                win(PLAYER_LEFT)

        # update the time
        update_time(False)

        if((curr_time - previous_time) == 0):
            #increase the speed
            ball_speed = ball_speed * 1.1

        update()

        fps.update()

        this_time = time.time()
        if this_time - prev_time < TIME_DELAY:
            delay_amount = TIME_DELAY - (this_time - prev_time)
            # print("delaying for %f sec" % delay_amount)
            time.sleep(delay_amount)

except Exception as e: # Ignore any of the exceptions for now...
    print("Caught exception %r" % e)
    # raise e

print("Final score was {} - {}".format(*scores))
bye()

