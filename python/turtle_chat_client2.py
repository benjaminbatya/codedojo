#!/usr/bin/python3

# This is an example chat client written in python
# It uses TCP sockets and pygame

import turtle
import helper_funcs
from client_socket import ClientSocket

# Setup the client socket
host = turtle.textinput("Enter host address", ">>")
if not host:
    host = "localhost"

port = turtle.numinput("Enter host port", "Port:", 9009, 0, 65535)
if not port:
    port = 9009
port = int(port)

print(host, port)
socket = ClientSocket(host, port)

# Setup the graphics
WIN_WIDTH = 600
WIN_HEIGHT = 1000
FG_COLOR = 'black'
FONT = ('Ariel', 24, 'normal')
NUM_MESSAGES = 20

screen = helper_funcs.setup_graphics("DojoChat",
                                     WIN_WIDTH, WIN_HEIGHT)
# screen.bgcolor('black')
screen.bgpic('background.gif')  # Use a cool background picture instead of black

# make an input poller
key_poller = helper_funcs.KeyPoller(screen)

# Variables to use
prev_messages = []
current_message = ''
do_redraw = True
is_running = True

s = turtle.Shape("compound")
head = helper_funcs.poly_circle(FONT[1]/2)
s.addcomponent(head, "yellow", "black")
mouth = helper_funcs.poly_circle(FONT[1]/4, 90)
mouth = helper_funcs.rotate(mouth, -45)
s.addcomponent(mouth, "yellow", "red")
eye = helper_funcs.poly_circle(2)
leye = helper_funcs.translate(eye, 0, -8)
s.addcomponent(leye, "black", "black")
reye = helper_funcs.translate(eye, 0, 1)
s.addcomponent(reye, "black", "black")
nose = helper_funcs.translate(eye, 4, -4)
s.addcomponent(nose, "green", "green")
screen.addshape("smiley", s)

def handle_socket():
    global prev_messages, do_redraw

    while(socket.has_messages()):
        do_redraw = True
        msg = socket.get_message()
        # print(msg)
        prev_messages.append(msg)

def handle_input():
    global is_running, do_redraw
    global key_poller, current_message, socket

    for key in key_poller.released():
        do_redraw = True
        key_chr = helper_funcs.get_key_chr(key)

        if key_chr:                 # Add the character to the message
            current_message += key_chr
        elif key == 'Escape':       # Quit the app
            is_running = False
        elif key == 'Return':       # Send the message
            socket.send_message(current_message)
            prev_messages.append(current_message)
            current_message = ''
        elif key == 'BackSpace':    # Remove the last character
            current_message = current_message[:-1]

def draw_smiley(x, y):
    turtle.goto(x+4, y+FONT[1]*.75)
    turtle.shape("smiley")
    turtle.stamp()
    # turtle.penup()

    # turtle.circle()

def draw_message(msg, x, y):
    # Replace
    msg = msg.replace('10.0.0.22', 'MacAir')

    # Handle emoticons
    while True:
        idx = msg.find(":-)")
        if idx != -1:
            start = msg[0:idx]
            turtle.goto(x, y)
            turtle.write(start, font=FONT, move=True)

            (x, y) = turtle.pos()
            draw_smiley(x,y)

            x += FONT[1]

            rest = ''
            if idx+3 < len(msg):
                rest = msg[idx+3:]
            msg = rest
        else:
            turtle.goto(x, y)
            turtle.write(msg, font = FONT)
            break

def redraw():
    global do_redraw
    if do_redraw:
        do_redraw = False

        turtle.clear()

        turtle.pencolor(FG_COLOR)
        turtle.pensize(4)
        turtle.goto(-WIN_WIDTH/2-10, -WIN_HEIGHT/2 + 50)
        turtle.pendown()
        turtle.goto(WIN_WIDTH/2+10, -WIN_HEIGHT/2 + 50)
        turtle.penup()
        turtle.home()

        # Draw the current message
        turtle.goto(-WIN_WIDTH/2+5, -WIN_HEIGHT/2+10)
        out_str = current_message + '_'
        turtle.write(out_str, font = FONT)

        # Draw the last NUM_MESSAGES messages
        N = len(prev_messages)
        for i in range(N-1, N-1-NUM_MESSAGES, -1):
            if i < 0:
                break
            msg = prev_messages[i]
            x = -WIN_WIDTH/2 + 5
            y = -WIN_HEIGHT/2 + 10 + (N-i)*50
            draw_message(msg, x, y)

        turtle.home()

    turtle.update()

try:
    # Run the event loop
    while(is_running):

        handle_socket()

        handle_input()

        redraw()

    turtle.bye()

except Exception as e: # Ignore any of Tkinter's thrown exceptions for now...
    print("Caught exception %r" % e)
    raise e

finally:
    print("Disconnecting from the server. Bye!")
    socket.disconnect()