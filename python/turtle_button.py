#!/usr/bin/python3

import turtle
import math
from Session2 import helper_funcs

WIN_WIDTH = 1000
WIN_HEIGHT = 1000

helper_funcs.setup_graphics("Button example", WIN_WIDTH, WIN_HEIGHT)

class Button(turtle.Turtle):

    _created = False
    DEFAULT_NAME = "button_shape"
    DEFAULT_COLORS = (('grey', 'black'), ('red', 'darkred'))
    class STATE:
        DEFAULT = 0
        PRESSED = 1

    def default_shape():
        if not Button._created:
            p = helper_funcs.poly_rect(100, 50)
            s = turtle.Shape("polygon", p)
            turtle.addshape(Button.DEFAULT_NAME, s)
            Button._created = True

        return Button.DEFAULT_NAME

    def __init__(self, text="", shape=None):

        if not shape:
            shape = Button.default_shape()

        super().__init__(shape)

        # Don't have this turtle draw anything but its shape
        self.up()

        self._text = text
        # self.write(text, align="center")

        x, y = self.pos()
        x = x * self.screen.xscale
        y = y * self.screen.yscale
        anchor = {"left":"sw", "center":"s", "right":"se" }
        self._text_item = self.screen.cv.create_text(x-1, -y, text = self._text, anchor="s")
        # x0, y0, x1, y1 = self.cv.bbox(item)

        self.onclick(self.clicked, btn=1)
        self.onrelease(self.released, btn=1)

        self._state = None
        self.state(Button.STATE.DEFAULT)

    def state(self, s=0):
        if s != self._state:
            self._state = s
            self.color(*Button.DEFAULT_COLORS[self._state])

        return self._state

    def clicked(self, x, y):
        # print("Button was clicked")
        self.state(Button.STATE.PRESSED)

    def released(self, x, y):
        self.state(Button.STATE.DEFAULT)

    def x(self):
        return self.pos()[0]

    def y(self):
        return self.pos()[1]

    def setpos(self, x ,y):
        super().setpos(x, y)
        x, y = self.pos()
        x = x * self.screen.xscale
        y = y * self.screen.yscale

        # self.clearstamp(self._text_item)

        self._text_item = self.screen.cv.create_text(x-1, -y, text = self._text, anchor="s")


b = Button("click me")
b.setpos(200, 100)

key_poller = helper_funcs.KeyPoller()

# mouse_poller = helper_funcs.MousePoller()

is_running = True

try:
    while is_running:
        for key in key_poller.released():
            if key == "Escape":
                is_running = False

        turtle.update()

    turtle.bye()
except Exception as e:
    raise e
