#!/usr/bin/python3

from turtle import *
import time

from lib.helper_funcs import *

# this is meant to give a consistant frame rate
TARGET_FPS = 50

WIN_WIDTH = 800
WIN_HEIGHT = 600

def draw_background(scores, time_str):
    clear()

    # # Draw a background
    pencolor('white')
    # fillcolor(None)
    penup()
    goto(-WIN_WIDTH/2, -WIN_HEIGHT/2)
    pendown()
    goto(WIN_WIDTH/2, -WIN_HEIGHT/2+1, )
    penup()
    goto(-WIN_WIDTH/2, WIN_HEIGHT/2-1)
    pendown()
    goto(WIN_WIDTH/2, WIN_HEIGHT/2-1)
    penup()
    home()

    # Draw the strings at the top
    penup()
    fillcolor('white')
    pencolor('white')
    home()
    goto(-WIN_WIDTH/2, WIN_HEIGHT/2)
    write(scores[0], font = ('Times New Roman', 36, 'bold'))

    penup()
    home()
    goto(-50, WIN_HEIGHT/2)
    write(time_str, font = ('Times New Roman', 36, 'bold'))

    penup()
    home()
    goto(WIN_WIDTH/2-50, WIN_HEIGHT/2)
    write(scores[1], font = ('Times New Roman', 36, 'bold'))

    penup()
    home()


setup_graphics(WIN_WIDTH, WIN_HEIGHT+50*2)

screen = Screen()
screen.bgcolor('black')
screen.title("Pong for CodeDojo")

PADDLE_HALF_WIDTH = 10
PADDLE_HALF_HEIGHT = 40

BALL_RADIUS = 6

PADDLE_X = WIN_WIDTH / 2 - 50
PADDLE_MAX_Y = WIN_HEIGHT / 2 - PADDLE_HALF_HEIGHT
MAX_Y = WIN_HEIGHT/2 - BALL_RADIUS

PADDLE_BUFFER = PADDLE_HALF_HEIGHT + 10

PLAYER_LEFT = 0
PLAYER_RIGHT = 1

# define the paddle object
define_rect("paddle", PADDLE_HALF_WIDTH, PADDLE_HALF_HEIGHT)

# Create a blocker on the left side
paddles = [None, None]

paddles[PLAYER_LEFT] = Object("paddle")
paddles[PLAYER_LEFT].fillcolor('grey')
paddles[PLAYER_LEFT].pencolor('white')
paddles[PLAYER_LEFT].setpos(-PADDLE_X, 0)

# create a blocker on the right side
paddles[PLAYER_RIGHT] = Object("paddle")
paddles[PLAYER_RIGHT].fillcolor('grey')
paddles[PLAYER_RIGHT].pencolor('white')
paddles[PLAYER_RIGHT].setpos(PADDLE_X, 0)

# Define the ball object
define_circle("ball", BALL_RADIUS)

s = Shape("compound", None)

# create the ball
ball = Object("ball")
ball.fillcolor('green')
ball.pencolor('darkgreen')
ball.setpos(0, 0)

# setup the ball's initial movement vector
ball_speed = Vec2D(-1, -1)

paddle_speed = 2

key_poller = KeyPoller(screen)

# indicates if the game is still running or not
is_running = True

# The current scores
scores = [0, 0]

def win(player):
    '''
    This updates the game when a player wins a volley
    :param player: the player that won the volloey
    :return: None
    '''
    global scores
    global ball, ball_speed
    global paddles, paddle_speed

    #reset the ball state
    ball.setpos(0, 0)
    if(player == PLAYER_LEFT):
        print("Left Player wins!")
        ball_vec = Vec2D(-1, -1)
    else:
        print("Right Player wins!")
        ball_vec = Vec2D(1, -1)

    # reset the paddle state
    for p in paddles:
        p.setpos(p.x(), 0)
    paddle_speed = 2

    # update the score of the winning player
    scores[player] += 1

    # reset the time
    update_time(True)

# The various time states
start_time = 0
previous_time = 0
curr_time = 0
def update_time(reset):
    '''
    This updates the displayed time
    :param reset: whether to reset the clock to zero or not
    :return: None
    '''
    global previous_time, start_time, curr_time
    time_str = "00 secs"
    update = False
    if reset:
        elapsed_time = start_time = time.time()
        update = True
    else:
        curr_time = time.time()
        if curr_time - elapsed_time > 1:
            secs = int(curr_time - start_time)
            time_str = '{:02d} secs'.format(secs)
            update = True
            elapsed_time = curr_time

    if update:
        global scores
        score_strs = [str(s) for s in scores]
        draw_background(score_strs, time_str)

# Always reset the time at the start
update_time(True)

# Display an FPS counter
fps = FPS("FPS")

try:
    while(is_running):

        # check the down keys
        for key in key_poller.keys():
            if(key == 'q'):
                pos = paddles[PLAYER_LEFT].pos() + (0, paddle_speed)
                paddles[PLAYER_LEFT].setpos(pos)
            elif key == 'a':
                pos = paddles[PLAYER_LEFT].pos() + (0, -paddle_speed)
                paddles[PLAYER_LEFT].setpos(pos)
            elif key == 'p':
                pos = paddles[PLAYER_RIGHT].pos() + (0, paddle_speed)
                paddles[PLAYER_RIGHT].setpos(pos)
            elif key == 'l':
                pos = paddles[PLAYER_RIGHT].pos() + (0, -paddle_speed)
                paddles[PLAYER_RIGHT].setpos(pos)
            elif key == 'Escape':
                is_running = False

        # Update the physics
        ball.setpos(ball.pos() + ball_speed)

        # increase the velocity of the ball and paddles once a second
        if((curr_time - previous_time) == 0):
            #increase the speed
            ball_speed = ball_speed * 1.1
            paddle_speed *= 1.1

        # check limits
        # First the y limits on the paddles
        if paddles[PLAYER_LEFT].y() < -PADDLE_MAX_Y:
            paddles[PLAYER_LEFT].setpos(paddles[PLAYER_LEFT].x(), -PADDLE_MAX_Y)
        elif paddles[PLAYER_LEFT].y() > PADDLE_MAX_Y:
            paddles[PLAYER_LEFT].setpos(paddles[PLAYER_LEFT].x(), PADDLE_MAX_Y)

        if paddles[PLAYER_RIGHT].y() < -PADDLE_MAX_Y:
            paddles[PLAYER_RIGHT].setpos(paddles[PLAYER_RIGHT].x(), -PADDLE_MAX_Y)
        elif paddles[PLAYER_RIGHT].y() > PADDLE_MAX_Y:
            paddles[PLAYER_RIGHT].setpos(paddles[PLAYER_RIGHT].x(), PADDLE_MAX_Y)

        # Then on the ball
        if(ball.y() <= -MAX_Y or MAX_Y <= ball.y()):
            ball_speed = Vec2D(ball_speed[0], ball_speed[1] * -1)

        if(ball.x() <= -PADDLE_X):
            #Check if the ball hits the paddle
            if (paddles[PLAYER_LEFT].y()-PADDLE_BUFFER <= ball.y() and \
                ball.y() <= paddles[PLAYER_LEFT].y()+PADDLE_BUFFER ):
                ball_speed = Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                win(PLAYER_RIGHT)
        elif(ball.x() >= PADDLE_X):
            #Check if the ball hits the paddle
            if (paddles[PLAYER_RIGHT].y()-PADDLE_BUFFER <= ball.y() and \
                ball.y() <= paddles[PLAYER_RIGHT].y()+PADDLE_BUFFER ):
                ball_speed = Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                win(PLAYER_LEFT)

        # update the displayed time
        update_time(False)

        update()

        fps.update()

        # Add a delay to make the game play smoother
        add_delay(TARGET_FPS)

    print("Final score was {} - {}".format(*scores))
    bye()

except Exception as e: # Ignore any of the exceptions for now...
    print("Caught exception %r" % e)
    # raise e


