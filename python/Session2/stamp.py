#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import helper_funcs

def shape_poly(size=100):
    # draw an equilateral triangle
    poly = ((-40, -30), (40, -30), (0, 50))
    s = turtle.Shape("polygon", poly)
    return s

def shape_compound(size=5):
    size /= 2
    square = ((-1, 1), (1, 1), (1, -1), (-1, -1))

    # Draw a tree looking object
    s = turtle.Shape("compound")

    poly = square
    scale_x, scale_y = 2*size, 10*size
    poly = helper_funcs.scale(poly, scale_x, scale_y)
    poly = helper_funcs.translate(poly, 0, 5*size)
    s.addcomponent(poly, "brown4", "brown")

    poly = square
    scale_x, scale_y = 0.5*size, 4*size
    poly = helper_funcs.scale(poly, scale_x, scale_y)
    poly = helper_funcs.rotate(poly, -45)
    poly = helper_funcs.translate(poly, 5*size, 10*size)
    s.addcomponent(poly, "brown4", "brown")

    poly = square
    scale_x, scale_y = 0.5*size, 4*size
    poly = helper_funcs.scale(poly, scale_x, scale_y)
    poly = helper_funcs.rotate(poly, 45)
    poly = helper_funcs.translate(poly, -5*size, 10*size)
    s.addcomponent(poly, "brown4", "brown")

    poly = helper_funcs.poly_circle(20)
    poly = helper_funcs.translate(poly, 0, 20*size)
    s.addcomponent(poly, "green", "lightgreen")

    return s

def shape_image(path="River.gif"):
    # Load that image on path. I believe that it has to be a GIF!
    s = turtle.Shape("image", path)
    return s

screen = helper_funcs.setup_graphics("Stamp example for CodeDojo", 1000, 1000)

# make different shapes and register them
shapes = []
s = shape_poly()
screen.addshape('left', s)
shapes.append(s)

s = shape_compound()
screen.addshape('middle', s)
shapes.append(s)

s = shape_image()
screen.addshape('right', s)
shapes.append(s)

# Create a set of colors to stamp with
colors = ['red', 'green', 'blue', '']

# Create a key poller
key_poller = helper_funcs.KeyPoller()

# Create a mouse poller
mouse_poller = helper_funcs.MousePoller()

# have a currently selected color and shape
color_idx = 0
turtle.fillcolor(colors[color_idx])   # Set a color

try:
    while True:
        # Look at the list of keys that were released
        keys = key_poller.released()
        for key in keys:
            if key == 'c':
                print("clearing the screen")
                turtle.clear()
            elif key == 'space':
                color_idx = (color_idx+1) % len(colors)
                turtle.fillcolor(colors[color_idx])   # Set a color
                print("set fillcolor to '%s'" % turtle.fillcolor())

        # Get all of the mouse events that happened since the last check
        mouse_events = mouse_poller.pressed()
        for event in mouse_events:
            # print("Received event=%s" % str(event))
            button = event.button_name()     # use the button name
            turtle.shape(button)        # Set a shape
            turtle.goto(event.x, event.y)   # goto the event position
            turtle.stamp()                  # stamp the shape

        screen.update()

    turtle.bye()

except Exception as e:
    print("Ignoring exception %r" % e) # ignore these exceptions, turtle doesn't like running outside of mainloop
    # raise e   # Uncomment this if the exception looks important
