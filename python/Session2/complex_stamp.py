#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import math
import helper_funcs

# Interesting curves from https://elepa.files.wordpress.com/2013/11/fifty-famous-curves.pdf
# Curve #47
def trifoluim_curve(alpha=100):
    pts = []
    for theta in range(0, 180, 3):
        theta *= math.pi/180 # convert to radians
        sinT = math.sin(theta)
        cosT = math.cos(theta)
        r = alpha * cosT * (4 * sinT**2 - 1)

        # Convert to x,y coordinates for drawing
        x = r * cosT
        y = r * sinT
        pts.append((x,y))

    return pts

# Curve #38
def rhodonea_curve(alpha=100, k=6):
    limit = 180
    if k%2==0:
        limit = 360

    pts = []

    for theta in range(0, limit, 3):
        theta *= math.pi/180 # convert to radians
        sinkT = math.sin(k * theta)
        r = alpha * sinkT

        # Convert to x,y coordinates for drawing
        x = r * math.cos(theta)
        y = r * math.sin(theta)

        pts.append((x,y))

    return pts

# Curve #16
def epicycloid_curve(a=50, b=60):
    pts = []

    limit = 12 * 180
    for theta in range(0, limit, 4):
        theta *= math.pi/180 # convert to radians
        sinT = math.sin(theta)
        cosT = math.cos(theta)

        cosABTT = math.cos((a/b + 1)*theta)
        sinABTT = math.sin((a/b + 1)*theta)

        x = (a+b)*cosT - b*cosABTT
        y = (a+b)*sinT - b*sinABTT

        pts.append((x,y))

    return pts

screen = helper_funcs.setup_graphics("Stamp example for CodeDojo", 1000, 1000)

# Make a set of curves
curves = []
curves.append(trifoluim_curve())
curves.append(rhodonea_curve())
curves.append(epicycloid_curve())

# make shapes from them
shapes = []
for i in range(len(curves)):
    s = turtle.Shape("polygon", curves[i])
    screen.register_shape(str(i), s)

# Create a set of colors to stamp with
colors = ['red', 'green', 'blue', '']

# Create our pollers
key_poller = helper_funcs.KeyPoller()
mouse_poller = helper_funcs.MousePoller(watch_drag=True)

# have a currently selected color and shape
color_idx = 0

stamps = []
def do_stamp(shape_idx, x, y, angle, color):
    turtle.shape(str(shape_idx))        # Set a shape
    turtle.goto(x, y)                   # goto the position
    turtle.settiltangle(angle)          # Set the tilt angle
    turtle.fillcolor(colors[color])     # Set the color
    turtle.stamp()                      # stamp the shape

is_rotating = False

try:
    while True:
        # Check the keys that were released
        for key in key_poller.released():
            if key == 'c':
                print("clearing the screen")
                turtle.clear()
                stamps = []
            elif key == 'space':
                color_idx = (color_idx+1) % len(colors)
                print("set fillcolor to '%s'" % colors[color_idx])
            elif key == 'r':
                is_rotating = not is_rotating

        # Get all of the mouse events that happened since the last check
        mouse_events = mouse_poller.released()
        for event in mouse_events:
            # print("Received event=%s" % str(event))
            idx = event.button - 1             # use index to get a shape
            new_stamp = [idx, event.x, event.y, 0, color_idx]   # have to use a list and not tuple so angle can be modified
            do_stamp(*new_stamp)
            stamps.append(new_stamp)

        if is_rotating:
            turtle.clear()
            for s in stamps:
                s[3] += 0.5     # update the rotation angle
                do_stamp(*s)

        screen.update()

    turtle.bye()

except Exception as e:
    print("Ignoring exception %r" % e) # ignore these exceptions, turtle doesn't like running outside of mainloop
    # raise e   # Uncomment this if the exception looks important

