#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import helper_funcs

# Setup the graphics
screen = helper_funcs.setup_graphics("Move Turtle example for CodeDojo",
                                     1000, 1000, hide_turtle=False)

# Setup the turtle. Complete list of color names are at https://www.tcl.tk/man/tcl8.4/TkCmd/colors.htm
turtle.shape("turtle")
turtle.color("YellowGreen", "darkgreen")

# Setup the key poller
key_poller = helper_funcs.KeyPoller()

# Setup the drawing state
is_drawing = False
def toggle_drawing():
    global is_drawing
    if is_drawing: turtle.penup()
    else: turtle.pendown()
    is_drawing = not is_drawing
toggle_drawing()

# Setup to run the main loop
is_running = True

try:
    while is_running:
        held_keys = key_poller.held()
        # The different key values are at https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm
        for key in held_keys:
            if key == "Up":
                turtle.forward(0.3)
            elif key == "Down":
                turtle.backward(0.3)
            elif key == "Left":
                turtle.left(0.3)
            elif key == "Right":
                turtle.right(0.3)

        pressed_keys = key_poller.pressed()
        for key in pressed_keys:
            if key == "space":
                toggle_drawing()
            if key == "Escape":
                is_running = False

        # Update the screen
        screen.update()

    turtle.bye()

except Exception as e:
    print("Ignoring exception %r" % e) # ignore these exceptions, turtle doesn't like running outside of mainloop
    # raise e   # Uncomment this if the exception looks important
