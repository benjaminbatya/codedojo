#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import helper_funcs

# this example uses minidom to write SVG files
# documentation at https://docs.python.org/3/library/xml.dom.minidom.html#module-xml.dom.minidom
# examples are at http://www.boddie.org.uk/python/XML_intro.html
from xml.dom.minidom import getDOMImplementation
dom_impl = getDOMImplementation()

WIN_WIDTH = 1000
WIN_HEIGHT = 1000

OFFSET = turtle.Vec2D(WIN_WIDTH, WIN_HEIGHT) * 0.5

SVG_FILE_PATH = "saved.svg"

drawn_lines = []
class Line(object):
    def __init__(self, start, end, color):
        self.start = start
        self.end = end
        self.color = color

# This saves the drawn lines to a svg file
# Example of SVG line elements is at http://tutorials.jenkov.com/svg/line-element.html
def save_drawing(path=SVG_FILE_PATH):
    global dom_impl, drawn_lines
    newdoc = dom_impl.createDocument(None, "svg", None)

    top_element = newdoc.documentElement
    top_element.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
    top_element.setAttribute("version", '1.1')

    for line in drawn_lines:
        line_node = newdoc.createElement("line")
        start = line.start + OFFSET
        end = line.end + OFFSET
        line_node.setAttribute('x1', str(start[0]))
        line_node.setAttribute('y1', str(start[1]))
        line_node.setAttribute('x2', str(end[0]))
        line_node.setAttribute('y2', str(end[1]))
        line_node.setAttribute('stroke', str(line.color))
        line_node.setAttribute('stroke-width', '1')
        top_element.appendChild(line_node)

    with open(path, "w") as f:
        top_element.writexml(f, '', '\t', '\n')

# Setup the graphics
screen = helper_funcs.setup_graphics("Turtle Sketching example for CodeDojo", WIN_WIDTH, WIN_HEIGHT)

key_poller = helper_funcs.KeyPoller()

mouse_poller = helper_funcs.MousePoller(watch_drag=True)

# Setup to run the main loop
is_running = True

prev_drag = False
start_pt = turtle.Vec2D(0,0)

try:
    while is_running:
        pressed_keys = key_poller.pressed()
        for key in pressed_keys:
            if key == "s":
                save_drawing()
            if key == "Escape":
                is_running = False

        for event in mouse_poller.pressed():
            if event.button == helper_funcs.MouseEvent.LEFT:
                # print("pressed")
                start_pt = turtle.Vec2D(event.x, event.y)
                prev_drag = False

        for event in mouse_poller.dragged():
            if event.button == helper_funcs.MouseEvent.LEFT:
                # print("drag")
                # undo the previous drag
                if prev_drag:
                    for i in range(4):
                        turtle.undo()
                turtle.goto(start_pt)
                turtle.pendown()
                turtle.goto(event.x, event.y)
                turtle.penup()
                prev_drag = True

        for event in mouse_poller.released():
            if event.button == helper_funcs.MouseEvent.LEFT:
                # Only record the line if it's long enough
                end_pt = turtle.Vec2D(event.x, event.y)
                diff = end_pt - start_pt
                if diff*diff > 4:
                    # Record the line
                    # print("Adding line %s-%s" % (start_pt, end_pt))
                    color = turtle.pencolor()
                    drawn_lines.append(Line(start_pt, end_pt, color))

        # Update the screen
        screen.update()

    turtle.bye()

except Exception as e:
    print("Ignoring exception %r" % e) # ignore these exceptions, turtle doesn't like running outside of mainloop
    # raise e   # Uncomment this if the exception looks important
