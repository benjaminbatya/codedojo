
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import time
import math

def setup_graphics(title, width=1000, height=1000, hide_turtle=True):
    '''
    Helper function to setup the turtle graphics environment for interactivity
    :param width: the width of the screen
    :param height: the height of the screen
    :param hide_turtle: hides the turtle and makes the updates immediate
    :return: None
    '''
    turtle.Screen().mode('standard')
    turtle.setup(width, height, 0, 0)
    turtle.title(title)

    if hide_turtle:
        turtle.hideturtle()

    turtle.penup()
    turtle.tracer(0,0)
    turtle.speed(0)
    turtle.delay(0)
    turtle.left(90)     # By default point up
    turtle.colormode(255)

    return turtle.Screen()

class KeyPoller(object):
    '''
    A key polling class which uses Tkinter's event callbacks instead of turtle's.
    It gives more consistant results. The keysyms are defined by tcltk and are consistant
    '''
    def __init__(self, screen=turtle.Screen(), timeout=0.1):
        # Allow the screen to listen for keypress events
        screen.listen()
        self._held = []
        self._pressed = {}
        self._released = {}
        self._timeout = timeout

        screen.getcanvas().bind("<KeyPress>", self._down)
        screen.getcanvas().bind("<KeyRelease>", self._up)

    def _down(self, event):
        # print("down: keycode= %r, char = %r, keysym = %r" % (event.keycode, event.char, event.keysym))
        if not event.keysym in self._held:
            # the keysyms are described here https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm and
            # should be cross-platform
            self._held.append(event.keysym)

        self._pressed[event.keysym] = time.time()

    def _up(self, event):
        # print("up: keycode= %r, char = %r, keysym = %r" % (event.keycode, event.char, event.keysym))
        self._held.remove(event.keysym)

        self._released[event.keysym] = time.time()

    def pressed(self):
        curr_time = time.time()
        ret = []

        for key in self._pressed:
            if (curr_time - self._pressed[key]) > self._timeout:
                ret.append(key)

        for key in ret:
            del self._pressed[key]

        return ret

    def held(self):
        return self._held

    def released(self):
        curr_time = time.time()
        ret = []

        for key in self._released:
            if (curr_time - self._released[key]) > self._timeout:
                ret.append(key)

        for key in ret:
            del self._released[key]

        return ret

class MouseEvent(object):

    # buttons active for this mouse event
    NONE = 0    # No mouse button was pressed during the mouse event
    LEFT = 1    # Left mouse button was pressed
    MIDDLE = 2  # Middle mouse button was pressed
    RIGHT = 3   # Right mouse button was pressed

    FIELD_NAMES = {
        NONE: 'none',
        LEFT: 'left',
        MIDDLE: 'middle',
        RIGHT: 'right'
    }

    def __init__(self, button, x, y):
        self.button = button    # The bit field of the buttons pressed down during the event
        self.x = x
        self.y = y

    def button_name(self):
        '''
        Returns just the first button involved in this mouse event
        :return:
        '''
        return MouseEvent.FIELD_NAMES[self.button]

class MousePoller(object):
    '''
    A mouse poller
    '''
    def __init__(self, watch_drag=False, screen=turtle.Screen()):
        '''
        :param screen:
        :return:
        '''
        self._pressed = []
        self._dragged = []
        self._released = []

        self._currently_pressed = 0

        self._screen = screen

        # Register for the button press events
        self._screen.getcanvas().bind("<ButtonPress-1>", self._press)
        self._screen.getcanvas().bind("<ButtonPress-2>", self._press)
        self._screen.getcanvas().bind("<ButtonPress-3>", self._press)

        # Register for the button release events
        self._screen.getcanvas().bind("<ButtonRelease-1>", self._release)
        self._screen.getcanvas().bind("<ButtonRelease-2>", self._release)
        self._screen.getcanvas().bind("<ButtonRelease-3>", self._release)

        if watch_drag:
            # Register for the button drag
            # NOTE: these only call _motion when the button is down. These might give better performance
            # self._screen.getcanvas().bind("<B1-Motion>", self._motion)
            # self._screen.getcanvas().bind("<B2-Motion>", self._motion)
            # self._screen.getcanvas().bind("<B3-Motion>", self._motion)

            # This watches ALL motion. May cause worst performance
            self._screen.getcanvas().bind("<Motion>", self._motion)

    def _press(self, event):
        # print("click: event = %r" % (event))
        (x, y) = (self._screen.cv.canvasx(event.x)/self._screen.xscale,
                 -self._screen.cv.canvasy(event.y)/self._screen.yscale)

        mouse_event = MouseEvent(event.num, x, y)
        self._pressed.append(mouse_event)

        btn_field = 1<<(event.num-1)
        self._currently_pressed |= btn_field

    def _release(self, event):
        # print("click: event = %r" % (event))
        (x, y) = (self._screen.cv.canvasx(event.x)/self._screen.xscale,
                 -self._screen.cv.canvasy(event.y)/self._screen.yscale)

        mouse_event = MouseEvent(event.num, x, y)
        self._released.append(mouse_event)

        btn_field = 1<<(event.num-1)
        self._currently_pressed &= ~btn_field

    def _motion(self, event):
        # print("_motion: event = %r" % (event))
        (x, y) = (self._screen.cv.canvasx(event.x)/self._screen.xscale,
                 -self._screen.cv.canvasy(event.y)/self._screen.yscale)

        added = False
        for i in range(3):
            btn = 1<<i
            if self._currently_pressed & btn:
                mouse_event = MouseEvent(i+1, x, y)
                self._dragged.append(mouse_event)
                added = True

        if not added:
            mouse_event = MouseEvent(MouseEvent.NONE, x, y)
            self._dragged.append(mouse_event)

    def pressed(self):
        ret = self._pressed
        self._pressed = []
        return ret

    def released(self):
        ret = self._released
        self._released = []
        return ret

    def dragged(self):
        ret = self._dragged
        self._dragged = []
        return ret

class Object(turtle.Turtle):
    '''
    creates an instance of the turtle shape that was registered with register_shape() or addshape()
    used to simplify working with turtle shapes in an interactive way
    '''
    def __init__(self, shape):
        super().__init__(shape)
        # Don't have this turtle draw anything but its shape
        self.up()
    def x(self):
        return self.pos()[0]
    def y(self):
        return self.pos()[1]

uniq_name = 0
def build_object(poly, fill_color, pen_color):
    '''
    Shape builder helper
    :param poly: the polygon to use in the definition
    :param fill_color: the polygon's fill color
    :param pen_color: the polygon's edge color
    :return: an instance of the shape as an Object
    '''
    s = turtle.Shape("compound", None)
    s.addcomponent(poly, fill_color, pen_color)
    global uniq_name
    name = str(uniq_name)
    uniq_name+=1
    # print(name)
    turtle.Screen().addshape(name, s)
    return Object(name)

def poly_rect(half_width, half_height):
    '''
    rectangle definition
    :param half_width: half width of the rectangle
    :param half_height: half height of the rectangle
    :return: the name of the rectangle shape which can be instanced using Object(name)
    '''
    poly = ((-half_width, -half_height), \
            (-half_width, half_height), \
            (half_width, half_height), \
            (half_width, -half_height))

    return poly

def poly_circle(radius):
    '''
    circle definition
    :param radius: radius of the circle
    :return: the name of the circle shape which can be instanced using Object(name)
    '''
    turtle.begin_poly()
    turtle.circle(radius)
    turtle.end_poly()
    poly = turtle.get_poly()

    poly = translate(poly, radius, 0)

    return poly

class FPS(object):
    '''
    Defines an object which tracks the running frames per second of the game and
    prints it once a second
    '''
    def __init__(self, name):
        self.name = name
        self.last_time = time.time()
        self.num_frames = 0

    def update(self):
        self.num_frames += 1
        curr_time = time.time()
        if(curr_time - self.last_time >= 1):
            print("%s: fps=%f" % (self.name, self.num_frames / (curr_time - self.last_time)))
            self.last_time = curr_time
            self.num_frames = 0

_prev_delay_time = time.time()

def add_delay(fps):
    '''
    This adds enough delay to the game loop to make the FPS constant.
    Only works if the game loop takes less time then 1/fps
    :param fps: the desired frames per second for the game
    :return: None
    '''
    global _prev_delay_time
    this_time = time.time()
    diff_time = this_time - _prev_delay_time

    delay_amount = 1.0 / fps
    if diff_time < delay_amount:
        delay_amount = delay_amount - diff_time
        # print("delaying for %f sec" % delay_amount)
        time.sleep(delay_amount)
    _prev_delay_time = time.time()

def translate(poly, offset_x, offset_y):
    '''
    copies poly and translates it by offset
    :param poly: poly to copy and translate
    :param offset: a tuple (x,y) of the offset
    :return: a translated copy of poly
    '''
    ret = []
    for pt in poly:
        x = pt[0] + offset_x
        y = pt[1] + offset_y
        ret.append((x,y))

    return ret

def rotate(poly, angle, origin=(0,0)):
    '''
    copies poly and rotates it
    :param poly: a list of (x,y) tuples
    :param angle: the angle to rotate by in degrees
    :param origin: optional origin to rotate through
    :return: rotated copy of poly
    '''
    angle *= math.pi / 180  # convert to radians

    # 2D rotation math is described at https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions
    cosA = math.cos(angle)
    sinA = math.sin(angle)
    ret = []
    for pt in poly:
        x = pt[0] - origin[0]
        y = pt[1] - origin[1]
        new_x = x*cosA - y*sinA + origin[0]
        new_y = x*sinA + y*cosA + origin[1]

        ret.append((new_x, new_y))

    return ret

def scale(poly, scale_x, scale_y, origin=(0,0)):
    '''
    copies poly and scales it
    :param poly: the polygon to use
    :param scale: a (x,y) tuple to scale by
    :param origin: optional origin to rotate through
    :return: scaled copy of poly
    '''
    ret = []
    for pt in poly:
        x = pt[0] - origin[0]
        y = pt[1] - origin[1]
        x = x*scale_x + origin[0]
        y = y*scale_y + origin[1]

        ret.append((x,y))

    return ret
