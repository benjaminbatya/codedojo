#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import math

class ComplexNumber(object):

    def __init__(self, r=0, i=0):
        self.r = r
        self.i = i

    def __add__(self, other):
        ret = ComplexNumber(self.r, self.i)
        ret.r += other.r
        ret.i += other.i
        return ret

    def __iadd__(self, other):
        self.r += other.r
        self.i += other.i
        return self

    def __sub__(self, other):
        ret = ComplexNumber(self.r, self.i)
        ret.r -= other.r
        ret.i -= other.i
        return ret

    def __isub__(self, other):
        self.r -= other.r
        self.i -= other.i
        return self

    def __mul__(self, other):
        ret = ComplexNumber()
        ret.r = self.r*other.r - self.i*other.i
        ret.i = self.r*other.i + self.i*other.r
        return ret

    def __imul__(self, other):
        temp = self.r*other.r - self.i*other.i
        self.i = self.r*other.i + self.i*other.r
        self.r = temp
        return self

    def __str__(self):
        return "%f+%fi" % (self.r, self.i)

    def sq(self):
        return self.r*self.r + self.i*self.i

    def mag(self):
        return math.sqrt(self.sq())