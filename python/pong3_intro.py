#!/usr/bin/python3

from turtle import *
import time
from lib.helper_funcs import *

# The game's screen size
WIN_WIDTH = 800
WIN_HEIGHT = 600

# Define the sizes of the game objects
PADDLE_HALF_WIDTH = 40
PADDLE_HALF_HEIGHT = 10
BALL_RADIUS = 6

PADDLE_Y = WIN_HEIGHT / 2 - 50

PADDLE_MAX_X = WIN_WIDTH / 2 - PADDLE_HALF_WIDTH

BALL_MAX_X = WIN_WIDTH/2 - BALL_RADIUS
BALL_MAX_Y = WIN_HEIGHT/2 - BALL_RADIUS

PADDLE_BUFFER = PADDLE_HALF_WIDTH + 10

# Set the time per frame to make smooth gameplay
# See add_delay below
DESIRED_FPS = 100

#do graphics setup
setup_graphics(WIN_WIDTH, WIN_HEIGHT+50*2)

#configure the screen
screen = Screen()
screen.bgcolor('black')
screen.title("Single Player Pong for CodeDojo")

# make an input poller
key_poller = KeyPoller(screen)

# define the paddle object
define_rect("paddle", PADDLE_HALF_WIDTH, PADDLE_HALF_HEIGHT)

# create an instance of the paddle, set its color and place it
paddle = Object("paddle")
paddle.fillcolor('grey')
paddle.pencolor('white')
paddle.setpos(0, -PADDLE_Y)

# Define the ball object
define_circle("ball", BALL_RADIUS)

# create a ball instance and set it up
ball = Object("ball")
ball.fillcolor('orange')
ball.pencolor('yellow')
ball.setpos(0, 0)

# setup the ball's initial movement vector
ball_speed = Vec2D(-1, -1)

# Setup the initial paddle speed
paddle_speed = 2

# Resets the game state when the ball is missed by the paddle
prev_best_time = 0
def miss_ball():
    global ball, ball_speed, start_time, prev_best_time, paddle_speed
    curr_time = time.time()
    secs = curr_time - start_time

    if prev_best_time < secs:
        time_str = 'You have achieved a new best time of {:04.2f} seconds!'.format(secs)
        prev_best_time = secs
    else:
        time_str = 'You lasted {:04.2f} seconds'.format(secs)

    print(time_str)

    # Reset the balls position and velocity
    ball.setpos(0, 0)
    ball_vec = Vec2D(-1, -1)
    paddle_speed = 2

    # Reset the time
    update_time(True)

# Handle updating the time string at the top of the screen
start_time = 0
previous_time = 0
curr_time = 0
def update_time(reset):
    global previous_time, start_time, curr_time
    time_str = "00 seconds"
    update = False
    if reset:
        elapsed_time = start_time = time.time()
        update = True
    else:
        curr_time = time.time()
        if curr_time - elapsed_time > 1:
            secs = curr_time - start_time
            time_str = '{:02.0f} seconds'.format(secs)
            update = True
            elapsed_time = curr_time

    if update:
        draw_background(time_str)

# We draw the background only occasionally because it's slow
def draw_background(time_str):
    clear()

    #Draw the top barrier here in red
    old_pen = pen()
    pen(fillcolor='red', pencolor='red', pensize=10)
    penup()
    goto(-WIN_WIDTH/2, WIN_HEIGHT/2-1)
    pendown()
    goto(WIN_WIDTH/2, WIN_HEIGHT/2-1)
    pen(old_pen)
    penup()
    home()

    # Draw the strings at the top
    fillcolor('white')
    pencolor('white')
    goto(-50, WIN_HEIGHT/2)
    write(time_str, font = ('Times New Roman', 36, 'bold'))
    penup()
    home()

# reset the time at the beginning of the game
update_time(True)

# Indicate that the game is running
is_running = True

try:
    # main game loop
    while(is_running):

        # check which keys are pressed
        # the keysyms are described here https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm and should be cross-platform
        for key in key_poller.keys():
            if(key == 'Left'):
                pos = paddle.pos() + (-paddle_speed, 0)
                paddle.setpos(pos)
            elif(key == 'Right'):
                pos = paddle.pos() + (paddle_speed, 0)
                paddle.setpos(pos)
            elif key == 'Escape':
                is_running = False

        # Update the ball position (physics)
        ball.setpos(ball.pos() + ball_speed)

        if((curr_time - previous_time) == 0):
            #increase the speed slowly to make the game more challenging the longer it lasts
            ball_speed = ball_speed * 1.05
            paddle_speed *= 1.05

        # check paddle limits
        # First the x limits on the paddles
        if paddle.x() < -PADDLE_MAX_X:
            paddle.setpos(-PADDLE_MAX_X, paddle.y())
        elif paddle.x() > PADDLE_MAX_X:
            paddle.setpos(PADDLE_MAX_X, paddle.y())

        # check the ball's limits
        if(ball.x() <= -BALL_MAX_X or BALL_MAX_X <= ball.x()):
            # Flip the direction that the ball is moving in
            ball_speed = Vec2D(ball_speed[0] * -1, ball_speed[1])

        if(ball.y() <= -PADDLE_Y):
            #Check if the ball hits the paddle
            if (paddle.x()-PADDLE_BUFFER <= ball.x() and \
                ball.x() <= paddle.x()+PADDLE_BUFFER ):
                ball_speed = Vec2D(ball_speed[0], ball_speed[1] * -1)
            else:
                miss_ball()
        elif(ball.y() >= BALL_MAX_Y):
            ball_speed = Vec2D(ball_speed[0], ball_speed[1] * -1)

        # update the time but don't reset
        update_time(False)

        # update the graphics display
        update()

        # Add a delay to make the game play smoother
        add_delay(DESIRED_FPS)

    # close the window
    bye()

except Exception as e: # Ignore any of Tkinter's thrown exceptions for now...
    print("Caught exception %r" % e)
    # raise e

print("Game over! Your best time was {:04.2f}".format(prev_best_time))
