from turtle import Vec2D

class Vec(object):
    """
    Designed copied from turtle.Vec2D but uses x/y instead and is mutable

    A 2 dimensional vector class, used as a helper class
    for implementing turtle graphics.
    May be useful for turtle graphics programs also.
    Derived from tuple, so a vector is a tuple!

    Provides (for a, b vectors, k number):
       a+b vector addition
       a-b vector subtraction
       a*b inner product
       k*a and a*k multiplication with scalar
       |a| absolute value of a
       a.rotate(angle) rotation
    """

    def __init__(self, x, y=None):
        self.x = 0
        self.y = 0
        if x is tuple:
            self.x = x[0]
            self.y = y[0]
        elif x is Vec:
            self.x = x.x
            self.y = x.y
        else:
            self.x = x
            self.y = y

    def __add__(self, other):
        return Vec(self.x+other.x, self.y+other.y)
    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self
    def __mul__(self, other):
        if isinstance(other, Vec):
            return self.x*other.x+self.y*other.y
        return Vec(self.x*other, self.y*other)
    def __imul__(self, other):
        self.x *= other
        self.y *= other
        return self
    def __rmul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return Vec(self.x*other, self.y*other)
    def __sub__(self, other):
        return Vec(self.x-other.x, self.y-other.y)
    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
    def __div__(self, other):
        return Vec(self.x/other, self.y/other)
    def __idiv__(self, other):
        self.x /= other
        self.y /= other
        return self
    def __neg__(self):
        return Vec(-self.x, -self.y)
    def __abs__(self):
        return (self.x**2 + self.y**2)**0.5
    def rotate(self, angle):
        """rotate self counterclockwise by angle
        """
        perp = Vec(-self.y, self.x)
        angle = angle * math.pi / 180.0
        c, s = math.cos(angle), math.sin(angle)
        return Vec(self.x*c+perp.x*s, self.y*c+perp.y*s)
    def __getnewargs__(self):
        return (self.x, self.y)
    def __repr__(self):
        return "(%.2f,%.2f)" % (self.x, self.y)
    def toVec2D(self):
        return Vec2D(self.x, self.y)