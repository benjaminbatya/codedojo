__author__ = 'benjamin'

class Signal(object):
    def __init__(self):
        self.slots = {}     # the slots is a one-to-many mapping

    def connect(self, func, with_sender=False):
        self.slots[func] = with_sender

    def remove(self, func):
        del self.slots[func]

    def emit(self, sender=None):
        for func, with_sender in self.slots.iteritems():
            if(with_sender):
                func(sender)
            else:
                func()