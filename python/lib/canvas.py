__author__ = 'benjamin'

import pygame

from widget import Widget

class Line(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def render(self, screen):
        pygame.draw.line(screen, (0, 0, 0), self.start, self.end)

class Canvas(Widget):
    def __init__(self, rect, font):
        super(Canvas, self).__init__()

        self.rect = rect
        self.font = font

        self.is_dragging = False
        self.drawing_objects = []

    def render(self, screen):
        pygame.draw.rect(screen, (255, 255, 255), self.rect)

        for obj in self.drawing_objects:
            obj.render(screen)

    def handle_event(self, event):
        if event.type == pygame.MOUSEMOTION:
            in_rect = self.rect.collidepoint(event.pos)
            if not self.has_focus:
                if in_rect:
                    # print("MOUSEMOTION")
                    self.has_focus = True
                    # self.enter_signal.emit(self)
                    return True
                else:
                    return False
            else:
                if not in_rect:
                    # self.exit_signal.emit(self)
                    self.has_focus = False
                    if self.is_dragging:
                        self.is_dragging = False
                    return False
                else:
                    if self.is_dragging:
                        pass
                    return True

        elif self.has_focus:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # print("MOUSEBUTTONDOWN")

                if not self.is_dragging:
                    pass


                return True
            elif event.type == pygame.MOUSEBUTTONUP:
                return True

        return False
