__author__ = 'benjamin'
import pygame
import time

from my_signal import Signal
from widget import Widget

DEFAULT_DOUBLE_CLICK_TIME = 500
def time_in_millis():
    return int(round(time.time() * 1000))

class Button(Widget):

    class MODE:
        DEFAULT = 0
        HIGHLIGHT = 1
        SELECT = 2

    FG_COLOR = (255, 255, 255)

    def __init__(self, text, rect, font):

        super(Button, self).__init__()

        self.text = text
        self.rect = rect
        self.font = font

        self.default_color = (45, 255, 45)
        self.highlight_color = (45, 45, 255)
        self.select_color = (255, 45, 45)

        self.state = Button.MODE.DEFAULT

        self.press_signal = Signal()
        self.release_signal=Signal()
        self.click_signal = Signal()
        self.double_click_signal = Signal()
        self.enter_signal = Signal()
        self.exit_signal  = Signal()

        self._last_click_time = time_in_millis()

    def render(self, screen):
        '''
        Draws a rectangle
        :param screen:
        :return:
        '''

        color = {
            Button.MODE.DEFAULT: self.default_color,
            Button.MODE.HIGHLIGHT: self.highlight_color,
            Button.MODE.SELECT: self.select_color
        }[self.state]

        pygame.draw.rect(screen, color, self.rect)
        pygame.draw.rect(screen, (0, 0, 0), self.rect, 1)
        label = self.font.render(self.text, 1, Button.FG_COLOR)
        screen.blit(label, (self.rect.x+5, self.rect.y))

    def handle_event(self, event):
        if event.type == pygame.MOUSEMOTION:
            in_rect = self.rect.collidepoint(event.pos)
            if not self.has_focus:
                if in_rect:
                    # print("MOUSEMOTION")
                    self.has_focus = True
                    self.state = Button.MODE.HIGHLIGHT
                    self.enter_signal.emit(self)
                    return True
                else:
                    return False
            else:
                if not in_rect:
                    self.state = Button.MODE.DEFAULT
                    self.exit_signal.emit(self)
                    self.has_focus = False
                    return False
                else:
                    return True

        elif self.has_focus:
            if event.type == pygame.MOUSEBUTTONDOWN:
                # print("MOUSEBUTTONDOWN")
                self.state = Button.MODE.SELECT
                self.press_signal.emit(self)
                return True
            elif event.type == pygame.MOUSEBUTTONUP:
                time = time_in_millis()
                # print("MOUSEBUTTONUP")
                self.state = Button.MODE.HIGHLIGHT

                self.release_signal.emit(self)

                # Figure out a single click verses double click
                # Ideally, use a timeout to detect if the double click occurs within the DEFAULT_DOUBLE_CLICK_TIME
                if((time-self._last_click_time) < DEFAULT_DOUBLE_CLICK_TIME):
                    self.double_click_signal.emit(self)
                else:
                    self._last_click_time = time
                    self.click_signal.emit(self)
                return True

        return False

