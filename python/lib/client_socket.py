__author__ = 'benjamin'
import socket
import sys
import threading
from collections import deque

class ClientSocket(threading.Thread):
    def __init__(self, addr, port, verbose=False):
        super(ClientSocket,self).__init__()
        self.addr = addr
        self.port = port
        self.verbose = verbose
        self._msg_queue = deque()

        self._do_run = True

        # Create and initialize the socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(0.1)
        try:
            self._socket.connect((self.addr, self.port))
        except:
            sys.stderr.write("Unable to connect to %s\n" % str(self.addr))
            sys.exit(-1)
        else:
            self._print("Connected to remote server %s" % str(self.addr))

        self.start()

    def run(self):

        while self._do_run:
            # Handle the socket input
            msg = None
            # print("Checking %s" % str(self.socket.getpeername()))
            try:
                msg = self._socket.recv(4096)
            except socket.error as e:
                err = e.args[0]
                # Ignore a time out error
                if err != 'timed out':
                    raise
            if msg:
                msg = msg.decode('UTF-8')
                self._print("Received '%s' from '%s'" % (msg, str(self._socket.getpeername())))
                self._msg_queue.append(msg)
                # print self.prev_messages

    def _print(self, msg):
        if(self.verbose):
            print(msg)

    def has_messages(self):
        return True if self._msg_queue else False

    def get_message(self):
        return self._msg_queue.popleft()

    def send_message(self, msg):
        self._socket.send(bytes(msg, 'UTF-8'))

    def get_local_addr(self):
        return str(self._socket.getsockname()[0])

    def get_remote_addr(self):
        return str(self._socket.getpeername())

    def disconnect(self):
        self._socket.sendall(bytes(chr(3), 'UTF-8'))

        self._do_run = False
        self.join()

