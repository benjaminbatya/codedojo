__author__ = 'benjamin'

import pygame
from my_signal import Signal

class BaseApp(object):
    def __init__(self):
        self.children = []
        self.active_child = None

        self.quit_signal = Signal()
        self.resize_signal = Signal()

    def setup(self, caption="", width=800, height=480):
        pygame.display.set_caption(caption)
        self.screen_props = pygame.locals.RESIZABLE|pygame.locals.HWSURFACE|pygame.locals.DOUBLEBUF

        self.screen = pygame.display.set_mode((width, height), self.screen_props)

        pygame.event.set_allowed(None)
        pygame.event.set_allowed([pygame.locals.QUIT,
                                  pygame.locals.KEYDOWN,
                                  pygame.MOUSEBUTTONDOWN,
                                  pygame.MOUSEBUTTONUP,
                                  pygame.MOUSEMOTION,
                                  pygame.locals.VIDEORESIZE])
        pygame.key.set_repeat(50, 50)

    def size(self):
        return self.screen.size()

    def handle_events(self):
        # pygame.event.pump()
        # print("checking pyevents")
        # Handle the game events
        for event in pygame.event.get():
            # print("event %s" % str(event))
            if event.type == pygame.QUIT:
                self.running = False
                self.quit_signal.emit(self)
                break
            elif event.type == pygame.VIDEORESIZE:
                self.screen = pygame.display.set_mode((event.w, event.h), self.screen_props)
                # size = self.screen.get_size()
                # print("new size=%s" % str(size))
                self.resize_signal.emit(self)
            elif event.type == pygame.MOUSEMOTION:
                if self.active_child and not self.active_child.handle_event(event):
                    self.active_child = None

                if not self.active_child:
                    for child in self.children:
                        if(child.handle_event(event)):
                            self.active_child = child
                            break

            elif event.type == pygame.MOUSEBUTTONUP or event.type == pygame.MOUSEBUTTONDOWN:
                if self.active_child:
                    self.active_child.handle_event(event)

    def run(self):
        self.running = True

        self.setup()

        try:
            while self.running:
                self.handle_events()

                self.update()

                self._render()
        finally:
            pass

    def update(self):
        pass

    def _render(self):

        self.render()

        # draw the children
        for b in self.children:
            b.render(self.screen)

        pygame.display.flip()

    def render(self):
        pass