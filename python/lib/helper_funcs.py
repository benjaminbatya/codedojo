from turtle import *
import time

class KeyPoller(object):
    '''
    A key polling class which uses Tkinter's event callbacks instead of turtle's.
    It gives more consistant results. The keysyms are defined by tcltk and are consistant
    '''
    def __init__(self, screen):
        # Allow the screen to listen for keypress events
        screen.listen()
        self._keys = []

        screen.getcanvas().bind("<KeyPress>", self._down)
        screen.getcanvas().bind("<KeyRelease>", self._up)

    def _down(self, event):
        # print("down: keycode= %r, char = %r, keysym = %r" % (event.keycode, event.char, event.keysym))
        if not event.keysym in self._keys:
            # the keysyms are described here https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm and
            # should be cross-platform
            self._keys.append(event.keysym)

    def _up(self, event):
        # print("up: key = %r" % event.char)
        self._keys.remove(event.keysym)

    def keys(self):
        '''
        :return: the list of pressed keysyms
        '''
        return self._keys

class Object(Turtle):
    '''
    creates an instance of the turtle shape that was registered with register_shape() or addshape()
    used to simplify working with turtle shapes in an interactive way
    '''
    def __init__(self, shape):
        super().__init__(shape)
        # Don't have this turtle draw anything but its shape
        self.up()
    def x(self):
        return self.pos()[0]
    def y(self):
        return self.pos()[1]

uniq_name = 0
def build_object(poly, fill_color, pen_color):
    '''
    Shape builder helper
    :param poly: the polygon to use in the definition
    :param fill_color: the polygon's fill color
    :param pen_color: the polygon's edge color
    :return: an instance of the shape as an Object
    '''
    s = Shape("compound", None)
    s.addcomponent(poly, fill_color, pen_color)
    global uniq_name
    name = str(uniq_name)
    uniq_name+=1
    # print(name)
    Screen().addshape(name, s)
    return Object(name)

def define_rect(name, half_width, half_height):
    '''
    rectangle definition
    :param name: name of the rectangle shape
    :param half_width: half width of the rectangle
    :param half_height: half height of the rectangle
    :return: the name of the rectangle shape which can be instanced using Object(name)
    '''
    poly = ((-half_width, -half_height), \
            (-half_width, half_height), \
            (half_width, half_height), \
            (half_width, -half_height))
    s = Shape("polygon", poly)
    Screen().addshape(name, s)

    return name

def define_circle(name, radius):
    '''
    circle definition
    :param name: name of the circle shape
    :param radius: radius of the circle
    :return: the name of the circle shape which can be instanced using Object(name)
    '''
    begin_poly()
    circle(radius)
    end_poly()
    ball_shape = get_poly()
    s = Shape("polygon", ball_shape)
    Screen().addshape(name, s)

    return name

def setup_graphics(width, height):
    '''
    Helper function to setup the turtle graphics environment for interactivity
    :param width: the width of the screen
    :param height: the height of the screen
    :return: None
    '''
    Screen().mode('logo')
    setup(width, height, 0, 0)
    hideturtle()
    tracer(0,0)
    speed(0)
    resizemode("noresize")

class FPS(object):
    '''
    Defines an object which tracks the running frames per second of the game and
    prints it once a second
    '''
    def __init__(self, name):
        self.name = name
        self.last_time = time.time()
        self.num_frames = 0

    def update(self):
        self.num_frames += 1
        curr_time = time.time()
        if(curr_time - self.last_time >= 1):
            print("%s: fps=%f" % (self.name, self.num_frames / (curr_time - self.last_time)))
            self.last_time = curr_time
            self.num_frames = 0

_prev_delay_time = time.time()
def add_delay(fps):
    '''
    This adds enough delay to the game loop to make the FPS constant.
    Only works if the game loop takes less time then 1/fps
    :param fps: the desired frames per second for the game
    :return: None
    '''
    global _prev_delay_time
    this_time = time.time()
    diff_time = this_time - _prev_delay_time

    delay_amount = 1.0 / fps
    if diff_time < delay_amount:
        delay_amount = delay_amount - diff_time
        # print("delaying for %f sec" % delay_amount)
        time.sleep(delay_amount)
    _prev_delay_time = time.time()