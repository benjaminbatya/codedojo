#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# Simple game to demonstrate how a game works

import turtle
import time
import helper_funcs

# The game's screen size
WIN_WIDTH = 800
WIN_HEIGHT = 600

# Define the sizes of the game objects
PADDLE_HALF_WIDTH = 5
PADDLE_HALF_HEIGHT = 40
BALL_RADIUS = 6

# Define the various limits
PADDLE_X = WIN_WIDTH / 2 - 10

PADDLE_MAX_Y = WIN_HEIGHT / 2 - PADDLE_HALF_HEIGHT

BALL_MAX_X = WIN_WIDTH/2 - BALL_RADIUS
BALL_MAX_Y = WIN_HEIGHT/2 - BALL_RADIUS

PADDLE_BUFFER = PADDLE_HALF_HEIGHT + 10

# Set the time per frame to make smooth gameplay
# See add_delay below
DESIRED_FPS = 100

# Setup the graphics screen and configure it
screen = helper_funcs.setup_graphics("Paddleball for CodeDojo. How long can you last?",
                                     WIN_WIDTH, WIN_HEIGHT+50*2)
screen.bgcolor('black')

# make an input poller
key_poller = helper_funcs.KeyPoller(screen)

# define the paddle object
poly = helper_funcs.poly_rect(PADDLE_HALF_WIDTH, PADDLE_HALF_HEIGHT)
s = turtle.Shape("polygon", poly)
screen.addshape("paddle", s)

# create an instance of the paddle, set its color and place it
paddle = helper_funcs.Object("paddle")
paddle.fillcolor('grey')
paddle.pencolor('white')
paddle.setpos(-PADDLE_X, 0)

# Create player 2
paddle2 = helper_funcs.Object("paddle")
paddle2.fillcolor("blue")
paddle2.pencolor("yellow")
paddle2.setpos(PADDLE_X, 0)

# Define the ball object
poly = helper_funcs.poly_circle(BALL_RADIUS)
s = turtle.Shape("polygon", poly)
screen.addshape("ball", s)

# create a ball instance, set its color and place it
ball = helper_funcs.Object("ball")
ball.fillcolor('orange')
ball.pencolor('yellow')

LEFT_PLAYER = 0
RIGHT_PLAYER = 1

def reinit_game_objects(player=LEFT_PLAYER):
    global ball, ball_speed, paddle_speed
    ball.setpos(0, 0)

    # setup the ball's initial movement vector
    if player == LEFT_PLAYER:    
        ball_speed = turtle.Vec2D(1, 1)
    else:
        ball_speed = turtle.Vec2D(-1, -1)

    # Setup the initial paddle speed
    paddle_speed = 2

reinit_game_objects()


# Resets the game state when the ball is missed by the paddle
counts = [0, 0]
prev_best_time = 0
def miss_ball(player):
    global start_time, prev_best_time, counts, is_running
    curr_time = time.time()
    secs = curr_time - start_time

    if prev_best_time < secs:
        time_str = 'You have achieved a new best time of {:04.2f} seconds!'.format(secs)
        prev_best_time = secs
    else:
        time_str = 'You lasted {:04.2f} seconds'.format(secs)

    print(time_str)

    counts[player] += 1

    if counts[LEFT_PLAYER] >= 3:
        print("Right player WINS!")
        is_running = False

    if counts[RIGHT_PLAYER] >= 3:
        print("Left player WINS!")
        is_running = False

    print("Losses = %r" % counts)

    reinit_game_objects(player)

    # Reset the time
    update_time(True)

# Handle updating the time string at the top of the screen
start_time = 0
previous_time = 0
curr_time = 0
def update_time(reset):
    global previous_time, start_time, curr_time
    time_str = "00 seconds"
    update = False
    if reset:
        previous_time = start_time = time.time()
        update = True
    else:
        curr_time = time.time()
        if curr_time - previous_time > 1:
            secs = curr_time - start_time
            time_str = '{:02.0f} seconds'.format(secs)
            update = True
            previous_time = curr_time

    if update:
        draw_background(time_str)

# We draw the background only occasionally because it's slow
def draw_background(time_str):
    global counts
    turtle.clear()

    old_pen = turtle.pen()

    # Draw two lines at top and bottom
    turtle.pen(fillcolor='red', pencolor='red', pensize=1)
    turtle.goto(-WIN_WIDTH/2, -WIN_HEIGHT/2)
    turtle.pendown()
    turtle.goto(WIN_WIDTH/2, -WIN_HEIGHT/2)
    turtle.penup()
    turtle.goto(-WIN_WIDTH/2, WIN_HEIGHT/2)
    turtle.pendown()
    turtle.goto(WIN_WIDTH/2, WIN_HEIGHT/2)
    turtle.penup()

    turtle.pen(old_pen)
    turtle.home()

    # Draw the strings at the top
    turtle.fillcolor('white')
    turtle.pencolor('white')
    turtle.goto(-50, WIN_HEIGHT/2)
    turtle.write(time_str, font = ('Times New Roman', 36, 'bold'))
    turtle.home()

    turtle.goto(-WIN_WIDTH/2+20, WIN_HEIGHT/2)
    count_str = "Wins=%d" % (counts[1])
    turtle.write(count_str, font = ('Times New Roman', 36, 'bold'))
    turtle.home()

    turtle.goto(WIN_WIDTH/2-200, WIN_HEIGHT/2)
    count_str = "Wins=%d" % (counts[0])
    turtle.write(count_str, font = ('Times New Roman', 36, 'bold'))
    turtle.home()

# reset the time at the beginning of the game
update_time(True)

# Indicate that the game is running
is_running = True

try:
    # main game loop
    while(is_running):

        # check which keys are pressed
        # the keysyms are described here https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm and should be cross-platform
        for key in key_poller.held():
            if(key == 'q'):
                pos = paddle.pos() + (0, paddle_speed)
                paddle.setpos(pos)

            if(key == 'a'):
                pos = paddle.pos() + (0, -paddle_speed)
                paddle.setpos(pos)

            if(key == 'p'):
                pos = paddle2.pos() + (0, paddle_speed)
                paddle2.setpos(pos)

            if(key == 'l'):
                pos = paddle2.pos() + (0, -paddle_speed)
                paddle2.setpos(pos)                
                
            if key == 'Escape':
                is_running = False

        # First the y limits on the paddles
        if paddle.y() < -PADDLE_MAX_Y:
            paddle.setpos(paddle.x(), -PADDLE_MAX_Y)
        elif paddle.y() > PADDLE_MAX_Y:
            paddle.setpos(paddle.x(), PADDLE_MAX_Y)

        # First the y limits on the paddles
        if paddle2.y() < -PADDLE_MAX_Y:
            paddle2.setpos(paddle2.x(), -PADDLE_MAX_Y)
        elif paddle2.y() > PADDLE_MAX_Y:
            paddle2.setpos(paddle2.x(), PADDLE_MAX_Y)

        # Update the ball position (physics)
        ball.setpos(ball.pos() + ball_speed)

        # check the ball's limits on top and bottom of the screen
        if(ball.y() <= -BALL_MAX_Y or BALL_MAX_Y <= ball.y()):
            # Flip the direction that the ball is moving in
            ball_speed = turtle.Vec2D(ball_speed[0], ball_speed[1] * -1)

        # Check if the paddle2 hits ball at the right of the screen
        if(ball.x() >= PADDLE_X):
            #Check if the paddle hits the ball
            if (paddle2.y()-PADDLE_BUFFER <= ball.y() and ball.y() <= paddle2.y()+PADDLE_BUFFER ):
                # the paddle hit the game so flip the direction of the ball's travel
                ball_speed = turtle.Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                # else, indicate that the ball was missed
                miss_ball(RIGHT_PLAYER)

        # Check if the paddle hits it at the left of the screen
        if(ball.x() <= -PADDLE_X):
            #Check if the paddle hits the ball
            if (paddle.y()-PADDLE_BUFFER <= ball.y() and ball.y() <= paddle.y()+PADDLE_BUFFER ):
                # the paddle hit the game so flip the direction of the ball's travel
                ball_speed = turtle.Vec2D(ball_speed[0] * -1, ball_speed[1])
            else:
                # else, indicate that the ball was missed
                miss_ball(LEFT_PLAYER)

        if(curr_time == previous_time):
            # Increase the ball and paddle speed every second to make the game more challenging the longer it lasts
            ball_speed = ball_speed * 1.05
            paddle_speed = paddle_speed * 1.05

        # update the time but don't reset
        update_time(False)

        # update the graphics display
        turtle.update()

        # Add a delay to make the game play smoother
        helper_funcs.add_delay(DESIRED_FPS)

    # End game loop

    # close the window
    turtle.bye()

except Exception as e: # Ignore any of Tkinter's thrown exceptions for now...
    print("Caught exception %r" % e)
    # raise e

print("Game over! Your best time was {:04.2f}".format(prev_best_time))
