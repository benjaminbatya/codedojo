
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

from turtle import Vec2D

# Make a vector
a = Vec2D(1, -1)
print(a)

# Add two vectors together
b = Vec2D(-2, 2)
v = a + b
print(v)

# Subtract two vectors
b = (-1, 1)
v = a - b
print(v)

# Form the dot product of the two vectors
b = Vec2D(-2, 2)
v = a * b
print(v)

# Scale vector a by 42
b = 42
v = a * b
print(v)


