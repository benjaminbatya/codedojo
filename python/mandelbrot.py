#!/usr/bin/python3

import turtle
import math
from Session2 import helper_funcs
from Session2.complex_number import ComplexNumber

WIN_WIDTH = 1000
WIN_HEIGHT = 1000

COMPLEX_MIN = ComplexNumber(-2, -1.5)
COMPLEX_MAX = ComplexNumber(+1, +1.5)
COMPLEX_ZERO = ComplexNumber(0, 0)

MAX_ITERATIONS = 15

def remap(x, y):
    '''
    Converts a position in the screen to a mapped Complex
    :param pos:
    :return:
    '''
    x = x/WIN_WIDTH + 0.5
    y = y/WIN_HEIGHT + 0.5
    x = x * (COMPLEX_MAX.r - COMPLEX_MIN.r) + COMPLEX_MIN.r
    y = y * (COMPLEX_MAX.i - COMPLEX_MIN.i) + COMPLEX_MIN.i
    return ComplexNumber(x, y)

def mandelbrot(c):
    z = COMPLEX_ZERO
    # max_z = COMPLEX_ZERO

    i = 0

    while i < MAX_ITERATIONS:
        z = z*z + c

        if z.sq() > 4:
            break

        i += 1

    return i

helper_funcs.setup_graphics("Mandelbrot example", WIN_WIDTH+100, WIN_HEIGHT+100)

# Cheat to speed up
turtle.bgcolor("black")

colors = ['black', 'DarkSlateGray', 'DarkGreen', 'MidnightBlue',
          'gainsboro', 'HotPink4', 'light coral', 'LightCyan3',
          'magenta2', 'maroon2', 'MediumAquamarine', 'firebrick',
          'OrangeRed', 'PaleGreen2', 'PeachPuff', 'white']

# Use the underlying tkinter functions to accelerate the drawing
cv = turtle.Screen().cv

for j in range(int(WIN_HEIGHT/2)):
    y = j
    # min_iters = 1000
    # max_iters = -1
    for i in range(WIN_WIDTH):
        x = i - WIN_HEIGHT/2
        c = remap(x, y)

        iters = mandelbrot(c)

        # min_iters = min(iters, min_iters)
        # max_iters = max(iters, max_iters)

        # print("At dot (%d,%d), iters = %d" %(x, y, iters))

        # Ignore black only
        if iters < 1:
            continue

        # c = "#%x%x%x" % (iters, iters, iters)
        c = colors[iters]
        # exploit the symmetry of the top level mandelbrot to halve the rendering time
        cv.create_line(x, y, x+1, y, fill=c)
        cv.create_line(x, -y, x+1,-y, fill=c)

    # print("min iters=%d, max iters=%d" % (min_iters, max_iters))
    turtle.update()

print("Done!")
turtle.exitonclick()

turtle.done()
