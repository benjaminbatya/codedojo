#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import sys
import json

WIN_WIDTH = 1000
WIN_HEIGHT = 1000

OFFSET = turtle.Vec2D(0,0)
SCALE = turtle.Vec2D(0,0)

def remap(pos):
    pos = turtle.Vec2D(pos[0], pos[1]) - OFFSET
    pos = turtle.Vec2D(pos[0]*SCALE[0], pos[1]*SCALE[1])
    return pos + (-WIN_WIDTH/2, WIN_HEIGHT/2)

def setup(points):
    global OFFSET, SCALE
    # turtle.Screen().mode('logo')
    turtle.setup(WIN_WIDTH, WIN_HEIGHT, 0, 0)
    turtle.hideturtle()
    # turtle.tracer(0, 0)
    turtle.delay(0)
    turtle.speed(0)
    turtle.resizemode("noresize")
    turtle.title("Connect the dots!")

    x_vals = [pt['dot_pos'][0] for pt in points] + [pt['num_pos'][0] for pt in points]
    y_vals = [pt['dot_pos'][1] for pt in points] + [pt['num_pos'][1] for pt in points]

    min_pos = turtle.Vec2D(min(x_vals), min(y_vals)) - (30, 30)
    max_pos = turtle.Vec2D(max(x_vals), max(y_vals)) + (30, 30)

    diff = max_pos - min_pos

    OFFSET = min_pos
    SCALE = turtle.Vec2D(1/diff[0]*WIN_WIDTH, -1/diff[1]*WIN_HEIGHT)

def draw_dots(points):

    turtle.penup()
    for dot in points:
        pos = dot['dot_pos']
        pos = remap(pos)
        turtle.goto(pos)
        turtle.dot(4, 'grey')

def draw_labels(points):
    turtle.penup()
    turtle.pencolor('grey')
    for pt in points:
        val = pt['index']
        pos = pt['num_pos']
        pos = turtle.Vec2D(pos[0], pos[1])
        pos = remap(pos)
        turtle.goto(pos - (0, 6))
        turtle.write(str(val), move=False, align="center", font=("Ariel", 6, "normal"))

def reveal_shape(points):

    turtle.delay(2)
    turtle.speed(1)
    turtle.home()

    turtle.pendown()
    turtle.showturtle()

    turtle.pencolor('black')

    # FIXME!!! Fix this in to reveal the hidden shape!
    for dot in points:
        pos = dot['dot_pos']
        pos = remap(pos)

if __name__ == '__main__':
    if(len(sys.argv) < 2):
        data_file_path = "connect_dots.json"
    else:
        data_file_path = sys.argv[1]

    points = None
    with open(data_file_path, "r") as f:
        points = json.load(f)

    setup(points)

    draw_dots(points)

    draw_labels(points)

    reveal_shape(points)

    turtle.done()