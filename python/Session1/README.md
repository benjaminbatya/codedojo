Session1 directions are at https://docs.google.com/document/d/1-3Fngea1Xdr3qKw5ypg-ZP85bgEmUQKLm1tI9ZOOqUk/edit?usp=sharing

Convert the code to PDF with: 
sudo apt-get install enscript
enscript -2 -C -j -Epython --color code.py -o - | ps2pdf - code.pdf

Print the PDFs with:
hp-print file.pdf
