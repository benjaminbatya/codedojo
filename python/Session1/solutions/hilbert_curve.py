#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle

screen = turtle.Screen()
painter = turtle.Turtle()
turtle.tracer(3)

# Production rules from https://en.wikipedia.org/wiki/Hilbert_curve
def draw_A(painter, n, len):
    if n>0:
        painter.left(90)
        draw_B(painter, n-1, len)
        painter.forward(len)
        painter.right(90)
        draw_A(painter, n-1, len)
        painter.forward(len)
        draw_A(painter, n-1, len)
        painter.right(90)
        painter.forward(len)
        draw_B(painter, n-1, len)
        painter.left(90)

def draw_B(painter, n, len):
    if n>0:
        painter.right(90)
        draw_A(painter, n-1, len)
        painter.forward(len)
        painter.left(90)
        draw_B(painter, n-1, len)
        painter.forward(len)
        draw_B(painter, n-1, len)
        painter.left(90)
        painter.forward(len)
        draw_A(painter, n-1, len)
        painter.right(90)

painter.pencolor("blue")
painter.penup()
painter.goto(-400, 400)
painter.pendown()

try:
    len = 1024
    iter = 8
    draw_B(painter, iter, len>>iter)

    turtle.done()
except Exception as e:
    print("Caught exception %r" % e)