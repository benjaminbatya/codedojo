#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle
import math

def draw_top_text():
    # Draw the top text
    turtle.pencolor("black")
    turtle.penup()
    turtle.goto(0, 220)
    turtle.write("Welcome to Turtle!", align="center", font=("Courier", 20, "bold"))

def draw_head():
    # Draw the head as an ellipse
    def calc_head_pos(i):
        i = (i + 1) / 60 * math.pi * 2
        x = math.cos(i) * 150
        y = math.sin(i) * 200
        return (x,y)

    turtle.color("brown", "brown4")
    turtle.goto(150, 0)
    turtle.pendown()
    turtle.begin_fill()
    for i in range(60):
        (x,y) = calc_head_pos(i)
        turtle.goto(x, y)
    turtle.end_fill()
    turtle.penup()

def draw_hair():
    # Draw the hair as a cos curve
    def calc_hair_y(x):
        return math.cos((x-100)/100 * math.pi)*10 + 130

    x = -150
    y = calc_hair_y(x)
    turtle.pencolor("DarkOliveGreen")
    turtle.goto(x, y)
    turtle.pendown()
    for i in range(31):
        x += 10
        y = calc_hair_y(x)
        turtle.goto(x, y)

    turtle.penup()

def draw_eyes():
    # Draw the eyes as circles
    def draw_eye():
        turtle.pendown()
        turtle.begin_fill()
        turtle.circle(20)
        turtle.end_fill()
        turtle.penup()

    turtle.pencolor("#0000ff")
    turtle.fillcolor("CornflowerBlue")
    turtle.goto(-50, 50)
    draw_eye()
    turtle.goto(50, 50)
    draw_eye()

def draw_nose():
    # Draw the nose as a square
    turtle.color("#a0a000", "#ffff00")
    turtle.goto(-10, 10)
    turtle.pendown()
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()
    turtle.penup()

def draw_mouth():
    # Draw the mouth as two cos curves filled in
    def calc_mouth(i, scale):
        x = i * 10 - 100
        y = math.cos((i - 10) / 20 * math.pi) * scale - 50
        return (x, y)

    (x,y) = calc_mouth(0, -10)
    turtle.goto(x, y)
    turtle.pendown()

    turtle.color("red", "red")
    turtle.begin_fill()

    for i in range(1, 21, 1):
        (x,y) = calc_mouth(i, -20)
        turtle.goto(x, y)

    for i in range(19, 1, -1):
        (x,y) = calc_mouth(i, -40)
        turtle.goto(x, y)

    turtle.end_fill()
    turtle.penup()

def draw_bottom_text():
    # Draw the bottom text
    turtle.pencolor("black")
    turtle.penup()
    turtle.goto(0, -250)
    turtle.write("Now you know how to draw pretty pictures like ME!", align="center", font=("Ariel", 20, "bold"))

draw_top_text()

draw_head()

draw_hair()

draw_eyes()

draw_nose()

draw_mouth()

draw_bottom_text()

# Park the turtle at the bottom
turtle.goto(0, -300)

turtle.done()