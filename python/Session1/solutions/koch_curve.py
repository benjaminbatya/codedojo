#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle

screen = turtle.Screen()
painter = turtle.Turtle()
turtle.tracer(1)

# Production rules from https://en.wikipedia.org/wiki/Koch_snowflake
def draw_curve(painter, n, len):
    if n==0:
        painter.forward(len)
    else:
        draw_curve(painter, n-1, len/3)
        painter.left(60)
        draw_curve(painter, n-1, len/3)
        painter.right(120)
        draw_curve(painter, n-1, len/3)
        painter.left(60)
        draw_curve(painter, n-1, len/3)

painter.pencolor("blue")
painter.penup()
painter.goto(-400, 400)
painter.pendown()

for i in range(3):
    draw_curve(painter, 4, 800)
    painter.right(120)

turtle.done()