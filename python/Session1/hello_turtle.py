#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

import turtle

def draw_top_text():
    # Draw the top text
    turtle.pencolor("black")
    turtle.penup()
    turtle.goto(0, 220)
    turtle.write("Welcome to Turtle!", align="center", font=("Courier", 20, "bold"))

def draw_head():
    # Draw the head as an circle

    turtle.color("brown", "brown4")
    turtle.goto(0, -200)
    turtle.pendown()
    turtle.begin_fill()
    turtle.circle(200)
    turtle.end_fill()
    turtle.penup()

def draw_hair():
    turtle.pencolor("DarkOliveGreen")
    turtle.goto(-170, 130)
    turtle.pendown()
    turtle.goto(-150, 150)
    turtle.goto(150, 150)
    turtle.goto(170, 130)
    turtle.penup()

def draw_eyes():
    # Draw the eyes as circles
    def draw_eye():
        turtle.pendown()
        turtle.begin_fill()
        turtle.circle(20)
        turtle.end_fill()
        turtle.penup()

    turtle.pencolor("#0000ff")
    turtle.fillcolor("CornflowerBlue")
    turtle.goto(-50, 50)
    draw_eye()
    turtle.goto(50, 50)
    draw_eye()

def draw_nose():
    # Draw the nose as a square
    turtle.color("#a0a000", "#ffff00")
    turtle.goto(-10, 10)
    turtle.pendown()
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()
    turtle.penup()

def draw_mouth():
    turtle.goto(-100, -80)
    turtle.pencolor("red")
    turtle.pensize(4)
    turtle.pendown()
    turtle.goto(-100, -100)
    turtle.goto(100, -100)
    turtle.goto(100, -80)
    turtle.penup()

def draw_bottom_text():
    # Draw the bottom text
    turtle.pencolor("black")
    turtle.penup()
    turtle.goto(0, -280)
    turtle.write("Now you know how to draw pictures!\n  Can you make me look better?", align="center", font=("Ariel", 20, "bold"))

# Draw the different parts of the head
draw_top_text()

draw_head()

draw_hair()

draw_eyes()

draw_nose()

draw_mouth()

draw_bottom_text()

# Park the turtle at the bottom
turtle.goto(0, -330)

turtle.done()