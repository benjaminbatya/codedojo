#!/usr/bin/python

import sys

n = 10
if len(sys.argv) > 1:
    n = int(sys.argv[1])

counts = [0]*10

for i in range(1, n+1):
    res = i
    while res > 0:
        digit = res % 10
        res = int(res/10)
        counts[digit] += 1

for i in range(10):
    print("Count of %d = %d" %(i, counts[i]))

