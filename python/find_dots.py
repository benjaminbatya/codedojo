#!/usr/bin/python

from matplotlib import pyplot as plt
import numpy as np
import cv2
import math
import os.path
import pickle
import sys
import json

class Digit(object):
    def __init__(self, digit, pos):
        self.digit = digit
        self.pos = pos

DISTANCE_INF = 100000000
def distance(a, b):
    diff = (a[0]-b[0], a[1]-b[1])
    return math.sqrt(diff[0]**2 + diff[1]**2)

class Number(object):
    def __init__(self, digit):
        self.digits = [digit]
        self.avg_y = digit.pos[1]
        self.min_x = digit.pos[0]
        self.max_x = digit.pos[0]

    def merge(self, that):
        self.digits += that.digits
        self.digits.sort(key = lambda d: d.pos[0])

        # recalc the average y value and min/max x values
        self.avg_y = reduce(lambda x, y: x + y, [d.pos[1] for d in self.digits]) / len(self.digits)
        self.min_x = min([d.pos[0] for d in self.digits])
        self.max_x = max([d.pos[0] for d in self.digits])

    def get_distance(self, that):
        # Checks that the digit is within 2 of self.avg_y
        if abs(that.avg_y - self.avg_y) > 2:
            return DISTANCE_INF

        # checks that the digit is within 15 of self.min_x or self.max_x
        if that.max_x+15 < self.min_x or self.max_x < that.min_x-15:
            return DISTANCE_INF
        else:
            # Just return the distance along the x axis between the two numbers
            return abs(that.max_x + that.min_x - (self.max_x + self.min_x)) / 2

    def value(self):
        total = 0
        for d in self.digits:
            total = total*10 + d.digit

        return total

    def pos(self):
        return ((self.max_x+self.min_x)/2, self.avg_y)

    def __str__(self):
        return "%r, %r" %(self.value(), self.pos())

    def __repr__(self):
        return self.__str__()

def find_matches(data):
    print("Finding matches in %s" % data['input'])
    img_orig = cv2.imread(data['input'], 0)
    output_result_img = cv2.cvtColor(img_orig, cv2.COLOR_GRAY2BGR)

    def find_template(img, template, threshold=0.8, min_dist=6):

        # following code from bottom of
        # http://docs.opencv.org/master/d4/dc6/tutorial_py_template_matching.html#gsc.tab=0
        res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)
        loc = np.where(res >= threshold)
        pts = []
        for pt in zip(*loc[::-1]):
            found = False

            # filter out duplicates
            for old_pt in pts:
                dist = distance(pt, old_pt)
                if(dist < min_dist):
                    # print("old_pt = %r, pt = %r" %(old_pt, pt))
                    found = True
                    break

            if not found:
                pts.append(pt)

        return pts

    def update_positions(positions, w, h):
        new_positions = []
        for pt in positions:
            # Adjust the dot position
            new_positions.append((pt[0]+w/2, pt[1]+h/2))

        return new_positions

    def display_matches(positions, w, h, img, color):
        '''
        Draws boxes around each match
        :param positions:
        :param w:
        :param h:
        :param img:
        :param color:
        :return:
        '''
        for pt in positions:
            cv2.line(img, (pt[0]-w/2, pt[1]-h/2), (pt[0]-w/2, pt[1]+h/2), color)
            cv2.line(img, (pt[0]-w/2, pt[1]+h/2), (pt[0]+w/2, pt[1]+h/2), color)
            cv2.line(img, (pt[0]+w/2, pt[1]+h/2), (pt[0]+w/2, pt[1]-h/2), color)
            cv2.line(img, (pt[0]+w/2, pt[1]-h/2), (pt[0]-w/2, pt[1]-h/2), color)

    def do_match(img, template_path, output_img, color, threshold):
        template = cv2.imread(template_path, 0)
        positions = find_template(img, template, threshold)
        w, h = template.shape[::-1]
        positions = update_positions(positions, w, h)
        display_matches(positions, w, h, output_img, color)
        print("found %d matches with template \'%s\'" % (len(positions), template_path))
        return positions

    matches = {}

    matches['dot'] = []
    for template in data['dot']:
        color = template.get('color', [255, 0, 0])
        threshold = template.get('threshold', 0.8)
        matches['dot'] += do_match(img_orig, template['file'], output_result_img, color, threshold)

    for i in range(10):
        key = str(i)
        matches[key] = []
        for template in data[key]:
            color = template.get('color', [0, 0, 255])
            threshold = template.get('threshold', 0.8)
            matches[key] += do_match(img_orig, template['file'], output_result_img, color, threshold)

    print("Writing results to %s" % data['visual'])
    cv2.imwrite(data['visual'], output_result_img)

        # plt.imshow(output_result_img)
        # plt.show()

        # cv2.imshow('original image', output_result_img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

    return (matches)

# builds a list of digits with with digit as (digit(0-9), pos)
def merge_digit_info(matches):
    out_list = []
    for i in range(10):
        key = str(i)
        for pos in matches[key]:
            out_list.append(Digit(i, pos))

    return out_list

def cluster_numbers(num_list):
    # Tracks a list of numbers which are a sorted list of digits. They sorted by x position.

    # This goes through the list of digits and for each digit and each number, checks if the digit is within
    # 1) 2 pixels of the number's average y value
    # 2) 15 pixels of the number's min and max x values
    # if both conditions are true, then the digit is added to the number in the x value

    # Maybe speed this up by using a kd-tree

    new_list = []
    for num in num_list:
        best_match = None
        best_dist = DISTANCE_INF
        for target in new_list:
            dist = target.get_distance(num)
            if dist < best_dist:
                best_match = target
                best_dist = dist

        if best_match:
            best_match.merge(num)
        else:
            new_list.append(num)

    return new_list

def main():

    data_file_path = 'assets/connect_dots/example1/input.json'
    if(len(sys.argv) > 1):
        data_file_path = sys.argv[1]

    print("loading json file %s" % data_file_path)

    with open(data_file_path, 'r') as f:
        data = f.readlines()
        data = ''.join(data)
        data = data.replace("\'", '"')
        data = json.loads(data)

    dir_path = os.path.dirname(data_file_path)
    data['input'] = os.path.join(dir_path, data['input'])
    data['visual'] = os.path.join(dir_path, data['visual'])
    data['temp'] = os.path.join(dir_path, data['temp'])

    for template_def in data['dot']:
        template_def['file'] = os.path.join(dir_path, template_def['file'])

    for i in range(10):
        key = str(i)
        for template_def in data[key]:
            template_def['file'] = os.path.join(dir_path, template_def['file'])

    data['final'] = os.path.join(dir_path, data['final'])

    if not os.path.isfile(data['temp']):
        matches = find_matches(data)
        digit_list = merge_digit_info(matches)
        match_data = (matches['dot'], digit_list)

        print("Writing match data to %s" % data['temp'])
        with open(data['temp'], 'wb') as f:
            pickle.dump(match_data, f, pickle.HIGHEST_PROTOCOL)
    else:
        print("Loading match data from %s" % data['temp'])
        with open(data['temp'], 'rb') as f:
            match_data = pickle.load(f)

    dot_list = match_data[0]
    digit_list = match_data[1]

    print("Num digits = %d" % (len(digit_list)))

    # Convert all of the digits to numbers
    num_list = []
    for d in digit_list:
        num_list.append(Number(d))

    # Loop until all of the numbers are merged
    # NOTE: maybe put a limit
    merge_count = 1
    while True:
        print("Merge round %d" % merge_count)
        merge_count += 1
        new_list = cluster_numbers(num_list)
        if len(new_list) == len(num_list):
            num_list = new_list
            break

        num_list = new_list

    print("Found %d numbers" % len(num_list))
    # for num in num_list:
    #     print(num)

    # Find the nearest number to each dot
    final_results = []
    for dot in dot_list:
        closest_num = None
        best_dist = DISTANCE_INF
        for num in num_list:
            dist = distance(dot, num.pos())
            if dist < best_dist:
                closest_num = num
                best_dist = dist

        final_results.append((closest_num, dot))
        # remove the best match from num_list once it is matched to a dot
        num_list.remove(closest_num)

    if len(num_list) != 0:
        raise Exception("All of the numbers should be used!")

    final_results.sort(key=lambda x: x[0].value())

    # for result in final_results:
    #     print("Num=%s, dot at %r" %(result[0], result[1]))

    print("writing match results to '%s'" % data['final'])
    out_list = []
    for match in final_results:
        obj = {
            'index': match[0].value(),
            'num_pos': match[0].pos(),
            'dot_pos': match[1],
        }
        out_list.append(obj)

    with open(data['final'], "w") as f:
        json.dump(out_list, f, indent=2)

if __name__ == '__main__':
    main()