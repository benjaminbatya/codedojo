# The rules are that the words are 6 letters, if the two words were if you flip the first letter,
# they are anytanyns of each other

word_list = """
    qwerty gwerty jweqtr vbgfdg gbgfdg
    asdfgh swerty
"""

with open('wordlist.txt', 'r') as f:
    word_list = f.readlines()

words = []
for l in word_list:
    l = l.strip()
    words += l.split()

# words = word_list.split()

# print(words)

def simp(w):
    # Remove the 3rd letter
    return w[:2] + w[3:]

matches = {}
for w in words:
    # rem = w[1:]
    rem = simp(w)
    if rem in matches:
        matches[rem].append(w)
    else:
        matches[rem] = [w]

valid = {}
for m in matches:
    l = matches[m]
    if len(l) > 1:
        valid[m] = l
        # out_str = ' '.join(l)
        # print(out_str)

with open("a-list.txt", 'r') as f:
    list = f.readlines()

for l in list:
    l = l.strip()
    w = l.split()
    a = w[0]
    b = w[2:]
    # print(a, b)

    # m = simp(a)
    # if m in valid:
    #     print(valid[m])

    for t in b:
        m = simp(t)
        if m in valid:
            print(valid[m])

# with open("a-list2.txt", 'r') as f:
#     list = f.readlines()
#
# for l in list:
#     l = l.strip()
#     l = l.replace(',', '')
#     w = l.split()
#
#     a = w[0]
#     r = w[1:]
#
#     m = simp(a)
#     if m in valid:
#         print(valid[m])

    # for t in r:
    #     m = simp(t)
    #     if m in valid:
    #         print(valid[m])

