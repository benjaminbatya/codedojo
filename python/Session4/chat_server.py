#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# chat_server.py

# Adapted from http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
 
import sys
import socket
import select

HOST = '' 
SOCKET_LIST = {}
# Strong limit on the number of characters allowed to be received
RECV_BUFFER = 256
PORT = 9009

def chat_server():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
 
    # add server socket object to the list of readable connections
    SOCKET_LIST[server_socket] = HOST + ':' + str(PORT)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com", 80))
    host_name = s.getsockname()[0]
    s.close()

    print("Chat server started on at %s on port %d" % (host_name, PORT))
 
    while 1:

        # get the list sockets which are ready to be read through select
        # 4th arg, time_out  = 0 : poll and never block
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:

            # a new connection request received
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST[sockfd] = addr

                full_addr = '[' + addr[0] + ':' + str(addr[1]) + ']'
                print("Client %s connected" % full_addr)
                broadcast(server_socket, sockfd, "%s has joined the chat" % addr[0])
             
            # a message from a client, not a new connection
            else:
                try:
                    # receiving data from the socket.
                    data = sock.recv(RECV_BUFFER)

                    # there is something in the socket
                    if data:
                        data = data.decode('UTF-8')

                        if(len(data) >= RECV_BUFFER):
                            # The client has tried to send too much data
                            sock.send(bytes("You are trying to send too much data.\nPlease don't hack this network. Bye", 'UTF-8'))
                            close_socket(server_socket, sock)

                        else:
                            # process data recieved from client,
                            addr = SOCKET_LIST[sock]
                            full_addr = '[' + addr[0] + ':' + str(addr[1]) + ']'
                            print("Received '%s' from %s" %(data, full_addr))
                            broadcast(server_socket, sock, addr[0] + ': ' + data)
                    else:
                        # at this stage, no data means probably the connection has been broken
                        close_socket(server_socket, sock)

                # exception 
                except:
                    close_socket(server_socket, sock)
                    continue

    server_socket.close()

def close_socket(server_socket, socket):
    print("Broken socket %s" % socket)
    # broken socket connection
    socket.close()
    # broken socket, remove it
    if socket in SOCKET_LIST:
        addr = SOCKET_LIST[socket]
        del SOCKET_LIST[socket]
        broadcast(server_socket, socket, "%s left the chat" % addr[0])

# broadcast chat messages to all connected clients
def broadcast (server_socket, sock, message):
    # print("Sending %s from sock = %s" % (message, str(sock.getpeername())))

    for socket in SOCKET_LIST:
        # print("Socket %s" % str(socket.getpeername()))
        # send the message only to peer
        if socket != server_socket: # and socket != sock:
            try :
                # print("Broadcasting %s to %s" % (message, str(socket.getpeername())))
                socket.send(bytes(message, 'UTF-8'))
            except :
                close_socket(server_socket, socket)

if __name__ == "__main__":

    if len(sys.argv) > 1:
        PORT = int(sys.argv[1])

    sys.exit(chat_server())   
