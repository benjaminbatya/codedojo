#!/usr/bin/python3


# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# This is an example chat client written in python
# It uses TCP sockets and turtle

import turtle
import helper_funcs

# Setup the client socket
server_address = "127.0.0.1" # Change this to the correct address
port = 9009 # Change this to the correct port number
socket = helper_funcs.ChatClient(server_address, port)

# Setup the graphics
WIN_WIDTH = 600
WIN_HEIGHT = 800
FG_COLOR = 'black'
FONT = ('Ariel', 24, 'normal')
NUM_MESSAGES = 20

screen = helper_funcs.setup_graphics("DojoChat", WIN_WIDTH, WIN_HEIGHT)
# screen.bgcolor('white')
screen.bgpic('background.gif')  # Use a cool background picture instead of black

# make an input poller
key_poller = helper_funcs.KeyPoller(screen)

# Variables to use
prev_messages = []
current_message = ''
do_redraw = True
is_running = True

# Define a smiley shape
s = turtle.Shape("compound")

head = helper_funcs.poly_circle(FONT[1]/2)
s.addcomponent(head, "yellow", "black")

mouth = helper_funcs.poly_circle(FONT[1]/4, 90)
mouth = helper_funcs.rotate(mouth, 45+180)
mouth = helper_funcs.translate(mouth, 0, -1)
s.addcomponent(mouth, "yellow", "red")

eye = helper_funcs.poly_circle(FONT[1]/12)
leye = helper_funcs.translate(eye, -FONT[1]/6, FONT[1]/6+1)
s.addcomponent(leye, "black", "black")

reye = helper_funcs.translate(eye, FONT[1]/6, FONT[1]/6+1)
s.addcomponent(reye, "black", "black")

nose = helper_funcs.translate(eye, 0, -FONT[1]/12)
s.addcomponent(nose, "green", "green")

screen.addshape("smiley", s)

def draw_smiley(x, y):
    turtle.goto(x+FONT[1]/2, y+FONT[1]*0.75)
    turtle.shape("smiley")
    turtle.stamp()

    return(x+FONT[1]*3/2, y)

# CHANGE 5
# Define a frowny shape
s = turtle.Shape("compound")

head = helper_funcs.poly_circle(FONT[1]/2)
s.addcomponent(head, "red", "black")

mouth = helper_funcs.poly_circle(FONT[1]/4, 90)
mouth = helper_funcs.rotate(mouth, 45)
mouth = helper_funcs.translate(mouth, 0, -FONT[1]/2)
s.addcomponent(mouth, "yellow", "black")

eye = helper_funcs.poly_circle(FONT[1]/12)
leye = helper_funcs.translate(eye, -FONT[1]/6, FONT[1]/6+1)
s.addcomponent(leye, "black", "black")

reye = helper_funcs.translate(eye, FONT[1]/6, FONT[1]/6+1)
s.addcomponent(reye, "black", "black")

nose = helper_funcs.translate(eye, 0, -FONT[1]/12)
s.addcomponent(nose, "green", "green")

screen.addshape("frowny", s)

def draw_frowny(x, y):
    turtle.goto(x+FONT[1]/2, y+FONT[1]*0.75)
    turtle.shape("frowny")
    turtle.stamp()

    return(x+FONT[1]*3/2, y)

# Define a river shape
s = turtle.Shape("image", "River.gif")
screen.addshape("river", s)

def draw_river(x, y):
    width = 200
    height = 105
    turtle.goto(x+width/2, y+height/2)
    turtle.shape("river")
    turtle.stamp()

    return(x+width, y)

# Change 1
ip_name_mapping = { }
# Change #2
blocked = { }

def handle_socket():
    global prev_messages, do_redraw

    while(socket.has_messages()):
        do_redraw = True
        msg = socket.get_message()
        # print(msg)

        # CHANGE #1
        # Handle a /name command
        global ip_name_mapping
        ip = msg.split(":")[0]

        idx = msg.find("/name")
        if idx != -1:
            rest = msg[idx+len("/name"):]
            toks = rest.split()
            if len(toks) > 0:
                name = toks[0]
                ip_name_mapping[ip] = name

        # Check for ip->name replacement
        for ip in ip_name_mapping:
            msg = msg.replace(ip, ip_name_mapping[ip])

        # CHANGE #2
        # Check for blocked ip
        global blocked
        add_msg = True
        for name in blocked:
            if msg.find(name) == 0:
                add_msg = False
                break

        if add_msg:
            prev_messages.append(msg)

def handle_input():
    global is_running, do_redraw
    global key_poller, current_message, socket
    global blocked_ips

    for keysym in key_poller.released():
        do_redraw = True
        key_ascii = helper_funcs.get_ascii(keysym)

        if key_ascii:                 # Add the character to the message
            current_message += key_ascii
        elif keysym == 'Escape':       # Quit the app
            is_running = False
        elif keysym == 'Return':       # Send the message

            # CHANGE 2
            # Handle /block command
            idx = current_message.find("/block")
            if idx != -1:
                rest = current_message[idx+len("/block"):]
                toks = rest.split()
                if len(toks) > 0:
                    name = toks[0]
                    blocked[name] = True

            # Handle /unblock command
            idx = current_message.find("/unblock")
            if idx != -1:
                rest = current_message[idx+len("/unblock"):]
                toks = rest.split()
                if len(toks) > 0:
                    name = toks[0]
                    if name in blocked:
                        del blocked[name]

            socket.send_message(current_message)
            current_message = ''
        elif keysym == 'BackSpace':    # Remove the last character
            current_message = current_message[:-1]

# CHANGE 3
EMOTICONS = {
    ":-)": draw_smiley,
    # Change 5
    ":-(": draw_frowny,
    # Change 6
    ":river": draw_river
}

def draw_message(msg, x, y):
    # CHANGE 3
    while len(msg) > 0:
        # Find first emoticon in msg
        found_icon = None
        icon_dist = len(msg)
        for e in EMOTICONS:
            idx = msg.find(e)
            if idx != -1 and idx < icon_dist:
                found_icon = e
                icon_dist = idx

        if found_icon:
            start = msg[0:icon_dist]

            turtle.goto(x, y)
            turtle.write(start, font = FONT, move=True)
            (x, y) = turtle.pos()
            func = EMOTICONS[found_icon]
            (x, y) = func(x, y)

            # Get the rest of the string
            offset = icon_dist+len(found_icon)
            msg = msg[offset:]

        else:
            turtle.goto(x, y)
            turtle.write(msg, font = FONT)
            break   # Done

def redraw():
    global do_redraw
    if do_redraw:
        do_redraw = False

        turtle.clear()

        # Draw the current message
        x = -WIN_WIDTH/2 + FONT[1]/4
        y = -WIN_HEIGHT/2 + FONT[1]/2
        turtle.goto(x, y)
        out_str = current_message + '_'
        turtle.write(out_str, font = FONT)

        # Draw the line between current_message and prev messages
        y += FONT[1]*2
        turtle.pencolor(FG_COLOR)
        turtle.pensize(FONT[1]/6)
        turtle.goto(-WIN_WIDTH/2-10, y)
        turtle.pendown()
        turtle.goto(WIN_WIDTH/2+10, y)
        turtle.penup()
        turtle.home()

        # Draw the last NUM_MESSAGES messages
        y += FONT[1]/2
        N = len(prev_messages)
        for i in range(N-1, N-1-NUM_MESSAGES, -1):
            if i < 0:
                break
            msg = prev_messages[i]
            draw_message(msg, x, y)
            y += FONT[1]*2

        # turtle.goto(-100, 0)
        # turtle.pd()
        # turtle.goto(100, 0)
        # turtle.pu()
        # turtle.goto(0, -100)
        # turtle.pd()
        # turtle.goto(0, 100)
        # turtle.pu()
        #
        # draw_river(0, 0)

        turtle.home()

    turtle.update()

try:
    # Run the event loop
    while(is_running):

        handle_socket()

        handle_input()

        redraw()

    turtle.bye()

except Exception as e: # Ignore any of Tkinter's thrown exceptions for now...
    print("Caught exception %r" % e)
    raise e

finally:
    print("Disconnecting from the server. Bye!")
    socket.disconnect()
