#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# chat_client.py

# Adapted From http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
import sys
import os

from lib.client_socket import ClientSocket

LOCAL_PROMPT = '[Me] '

def chat_client(host, port):

    try:
        socket = ClientSocket(host, port)

        print("Connected to remote host %s. Type in your message and press Enter to send it. Text 'quit' to quit." % socket.get_remote_addr())

        while True:

            while socket.has_messages():
                print(socket.get_message())

            sys.stdout.write(LOCAL_PROMPT)

            # user entered a message
            msg = sys.stdin.readline().strip()
            socket.send_message(msg)

            msg = msg.lower()
            if msg == 'quit' or msg == 'exit':
                break
    finally:
        socket.disconnect()


if __name__ == "__main__":
    if(len(sys.argv) < 3) :
        print("Usage : python chat_client.py hostname port)")
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])


    sys.exit(chat_client(host, port))
