#!/usr/bin/python2

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# This is an example chat client written in python
# It uses TCP sockets and pygame

import pygame
import pygame.locals
import time

from client_socket import ClientSocket

class ChatClient(object):
    def __init__(self, addr="127.0.0.1", serverport=9009):

        self.socket = ClientSocket(addr, serverport)

        self.setup_pygame()

    def run(self):
        self.running = True
        clock = pygame.time.Clock()
        tickspeed = 30

        self.input_buffer = ""

        self.prev_messages = []
        self.prev_messages.append("Welcome to ChatAlot. You are [%s] to your friends" % self.socket.get_local_addr())

        try:
            while self.running:
                clock.tick(tickspeed)

                while self.socket.has_messages():
                    self.prev_messages.append(self.socket.get_message())

                self.handle_pygame_events()

                self.draw_screen()
        finally:
            # Inform the server that this client is  disconnecting
            self.socket.disconnect()

    # START pygame related functionality

    FONT_SIZE = 20
    FG_COLOR = (255, 255, 255)
    INPUT_HEIGHT = FONT_SIZE + 5

    def setup_pygame(self, width=800, height=400):
        pygame.init()

        pygame.display.set_caption("CodeDojo ChatAlot App. Written in Python")
        self.screen_props = pygame.locals.RESIZABLE|pygame.locals.HWSURFACE|pygame.locals.DOUBLEBUF

        self.screen = pygame.display.set_mode((width, height), self.screen_props)
        self.bg_surface = pygame.image.load("../../assets/game_background.png").convert()

        # self.image = pygame.image.load("game_sprite.png").convert_alpha()

        # from http://stackoverflow.com/questions/10077644/python-display-text-w-font-color
        self.font = pygame.font.SysFont("monospace", self.FONT_SIZE)

        pygame.event.set_allowed(None)
        pygame.event.set_allowed([pygame.locals.QUIT,
                                  pygame.locals.KEYDOWN,
                                  pygame.locals.VIDEORESIZE])
        pygame.key.set_repeat(50, 50)

    SHIFT_INPUT = "`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./"
    SHIFT_OUTPUT= "~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:\"ZXCVBNM<>?"

    def handle_pygame_events(self):
        # pygame.event.pump()
        # print("checking pyevents")
        # Handle the game events
        for event in pygame.event.get():
            # print("event %s" % str(event))
            if event.type == pygame.QUIT or event.type == pygame.locals.QUIT:
                self.running = False
                break
            elif event.type == pygame.locals.VIDEORESIZE:
                self.screen = pygame.display.set_mode((event.w, event.h), self.screen_props)
                # size = self.screen.get_size()
                # print("new size=%s" % str(size))
            elif event.type == pygame.locals.KEYDOWN:
                if event.key == pygame.locals.K_ESCAPE:
                    self.running = False
                    break
                elif event.key == pygame.locals.K_RETURN:
                    if self.input_buffer == '': continue

                    print("sending '%s' to %s" % (self.input_buffer, str(self.socket.get_remote_addr())))
                    # Send the message to the socket
                    self.socket.send_message(self.input_buffer)
                    self.prev_messages.append('[ME] ' + self.input_buffer)
                    self.input_buffer = ""
                    pygame.event.clear(pygame.locals.KEYDOWN)

                elif event.key == pygame.locals.K_BACKSPACE: # event.key == pygame.locals.K_DELETE or
                    self.input_buffer = self.input_buffer[0:-1]

                elif pygame.locals.K_SPACE <= event.key and event.key <= pygame.locals.K_z:
                    ch = chr(event.key)
                    mods = pygame.key.get_mods()
                    if mods & pygame.locals.KMOD_SHIFT:
                        idx = self.SHIFT_INPUT.find(ch)
                        if(idx < 0):
                            print("Unknown character '%s', skipping" % ch)
                        else:
                            ch = self.SHIFT_OUTPUT[idx]

                    self.input_buffer += ch

    def draw_screen(self):

        (width, height) = self.screen.get_size()

        # Draw the display
        pygame.transform.scale(self.bg_surface, (width, height), self.screen)

        # Calculate the amount of space we have to display messages
        num_lines = int((height - self.INPUT_HEIGHT) / self.FONT_SIZE)

        # draw previous messages
        num_msg = len(self.prev_messages)
        for idx in range(num_lines):
            msg_idx = num_msg - num_lines + idx
            # print("msg_idx=" + str(msg_idx))
            if msg_idx < 0 or num_msg <= msg_idx: continue

            msg = self.prev_messages[msg_idx]
            label = self.font.render(msg, 1, self.FG_COLOR)
            self.screen.blit(label, (5, idx*self.FONT_SIZE))

        # Draw separating line
        pygame.draw.line(self.screen, self.FG_COLOR, (0, height-self.INPUT_HEIGHT), (width, height-self.INPUT_HEIGHT), 3 )

        # Draw a blinking cursor
        curr_time = int(time.time()*2)
        blink = (curr_time%2) == 0

        display_str = self.input_buffer
        if blink:
            display_str += "_"

        # Draw input area
        label = self.font.render(display_str, 1, self.FG_COLOR)
        self.screen.blit(label, (5, height - self.INPUT_HEIGHT))

        pygame.display.flip()

        # pygame.display.update()

    # END pygame related functionality

if __name__ == "__main__":
    g = ChatClient()
    g.run()
