#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in March, 2016.

# Adapted from http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
import sys
import time

from client_socket import ClientSocket
import collections
import threading

LOCAL_PROMPT = '>> '

_do_work = True
_queue = collections.deque()

def poll_input():
    global _do_work, _queue
    while _do_work:
        msg = sys.stdin.readline().strip()
        _queue.append(msg)

        msg = msg.lower()
        if msg == 'quit' or msg == 'exit':
            _do_work = False
            break

def chat_client(host, port):
    global _do_work, _queue

    try:
        socket = ClientSocket(host, port)

        t = threading.Thread(target=poll_input)
        t.start()

        print("Connected to remote host %s. Type in your message and press Enter to send it. Text 'quit' to quit." % socket.get_remote_addr())
        sys.stdout.write(LOCAL_PROMPT)
        sys.stdout.flush()

        while _do_work:

            if socket.has_messages():
                while socket.has_messages():
                    print(socket.get_message())

                sys.stdout.write(LOCAL_PROMPT)
                sys.stdout.flush()

            # user entered a message
            if _queue:
                while _queue:
                    socket.send_message(_queue.popleft())

                sys.stdout.write(LOCAL_PROMPT)
                sys.stdout.flush()

            time.sleep(0.1)

    finally:
        _do_work = False
        t.join()
        socket.disconnect()


if __name__ == "__main__":
    if(len(sys.argv) < 3) :
        print("Usage : python chat_client.py hostname port)")
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])


    sys.exit(chat_client(host, port))
