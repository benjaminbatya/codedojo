#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

hostname = "localhost"
host_port= 80

class MyServer(BaseHTTPRequestHandler):
    def write_line(self, output):
        self.wfile.write(bytes(output, "utf-8"))

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.write_line("""
<html>
    <head>
        <title>Title goes here.</title>
    </head>
    <body>
        <p>This is a test.</p>
        <p>You accessed path: %s</p>
    </body>
</html>
    """ % self.path)

my_server = HTTPServer((hostname, host_port), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostname, host_port))

try:
    my_server.serve_forever()
except KeyboardInterrupt:
    pass

my_server.server_close()
print(time.asctime(), "Server Ends - %s:%s" % (hostname, host_port))



