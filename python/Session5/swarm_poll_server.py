#!/usr/bin/python3


# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# Server setup code from
# http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
 
import sys
import socket
import select
import time
from random import shuffle

# The server starts on localhost which is the empty string by default
from numpy.lib.function_base import percentile

HOST = ''
# 9000 is the Default port for the server
PORT = 9000

SOCKET_LIST = {}
SERVER_SOCKET = None

# Strong limit on the number of characters allowed to be received to prevent flood attacks
RECV_BUFFER = 256

POLL_OPTIONS = [
    'Chocolate'
    , 'Vanilla'
    , 'Cookies and Cream'
    , 'Peanut Butter'
    , 'Rocky Road'
    , 'Strawberry'
]

POLL_VOTE = {}
OPTIONS_ORDER = {}
OPTIONS_REV_ORDER = {}

LAST_UPDATE = None

def gen_results(sock_addr, percentages):
    global OPTIONS_ORDER, POLL_OPTIONS
    msg = "Current percentages ="
    for idx in range(len(percentages)):
        map_idx = OPTIONS_ORDER[sock_addr][idx]
        msg += "\n%d: %s: %1.2f" % (idx+1, POLL_OPTIONS[map_idx], percentages[map_idx])

    return msg

def process_message(addr, message=""):
    '''
    This is a swarm poll server
    :param addr: address of the client that sent the message
    :param message: Message that the client sent. if it's an empty string, then this is the initial connection reply
    :return: The reply to the client
    '''

    global POLL_OPTIONS, POLL_VOTE, OPTIONS_ORDER, LAST_UPDATE
    # Do the swarm poll.
        # Inspiration is from http://unu.ai/wp-content/uploads/2016/02/Human-Swarming-AAAI-Rosenberg.pdf
        # How it
    reply = ""
    if message == "":
        # Generate a random ordering for the user to remove ordering bias
        if not addr in OPTIONS_ORDER:
            options = [a for a in range(len(POLL_OPTIONS))]
            shuffle(options)
            OPTIONS_ORDER[addr] = options
            #
            # print(options)

        reply = '''
Welcome to my server.
This server is all about finding the
best ice cream flavor.
You vote by typing in the number
of the flavor that you want to win.
If your flavor isn't winning,
try supporting one that you also like
The flavors are:'''
        options = OPTIONS_ORDER[addr]
        for idx in range(len(options)):
            reply += "\n%d: %s" % (idx+1, POLL_OPTIONS[options[idx]])

    else:
        options = OPTIONS_ORDER[addr]
        try:
            vote = int(message)-1
            # print("vote = ", vote)
            if(vote < 0 or vote >= len(POLL_OPTIONS)):
                reply = "%d is not a valid option!" % vote
            else:
                POLL_VOTE[addr] = options[vote]
                reply = "You have voted for %s" % POLL_OPTIONS[POLL_VOTE[addr]]
        except ValueError:
            reply = "'%s' is not a number!" % message

    # Recalculate the percentages
    votes = [0] * len(POLL_OPTIONS)
    for vote in POLL_VOTE.values():
        votes[vote] += 1
    # print(votes)
    total = len(SOCKET_LIST)-1
    # print(total)
    percentages = [val/total for val in votes]
    # print(percentages)

    reply += "\n" + gen_results(addr, percentages)

    # Once a second, broadcast out the current results to the other clients
    curr_time = time.time()
    # print("curr_time=", curr_time)
    if not LAST_UPDATE:
        LAST_UPDATE = curr_time
    if((curr_time - LAST_UPDATE) > 1):
        # print("Doing update broadcast")
        LAST_UPDATE = curr_time

        for sock in SOCKET_LIST:
            # Definitely don't try to send to the server
            if sock == SERVER_SOCKET:
                continue
            sock_addr = SOCKET_LIST[sock]
            # Skip the client, we already have generated the results for him
            if sock_addr == addr:
                continue

            msg = gen_results(sock_addr, percentages)

            send_reply(sock, msg)

    return reply

def chat_server():
    '''
    This runs the server in a forever loop. It gets connections from clients,
    opens sockets to them and then processes the replies in process_message()
    :return: None
    '''
    global SERVER_SOCKET, SOCKET_LIST
    # Setup the socket to use IPv4 and TCP (==STREAM)
    SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # This tells the socket to reuse the address
    SERVER_SOCKET.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Setup the socket to listen on the localhost and on PORT
    SERVER_SOCKET.bind((HOST, PORT))
    # Can listen to 50 connections at a time
    SERVER_SOCKET.listen(50)

    # figure out the server's IP address on the local network so clients can connect to it
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com", 80))
    host_name = s.getsockname()[0]
    s.close()
    server_address = host_name

    # add server socket object to the list of readable connections
    SOCKET_LIST[SERVER_SOCKET] = server_address

    print("Server started at %s and on port %d" % (server_address, PORT))

    while 1:

        # get the list sockets which are ready to be read through select
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST.keys(),[],[])
      
        for sock in ready_to_read:
            try:
                # a new connection request received
                if sock == SERVER_SOCKET:
                    # Accept the connection to bind the socket file descriptor to the address
                    # It will remain bound until the socket is closed
                    sockfd, addr = SERVER_SOCKET.accept()

                    # Save just the address for later
                    SOCKET_LIST[sockfd] = addr[0]
                    addr = SOCKET_LIST[sockfd]

                    reply = process_message(addr)
                    print("Client at %s connected" % addr)
                    send_reply(sockfd, reply)

                # a message from a client, not a new connection
                else:
                        # receiving data from the socket.
                        data = sock.recv(RECV_BUFFER)

                        # there is something in the socket
                        if data:
                            message = data.decode('UTF-8')

                            if(len(message) >= RECV_BUFFER):
                                # The client has tried to send too much data
                                send_reply(sock, "You are trying to send too much data.\nTry something else to hack this server. Goodbye")
                                raise RuntimeError("Hacking Attempt")

                            else:
                                # process the message from the client
                                addr = SOCKET_LIST[sock]
                                print("Received message '%s' from %s" % (message, addr))

                                reply = process_message(addr, message)
                                send_reply(sock, reply)
                        else:
                            # at this stage, no data means probably the connection has been broken so close the socket
                            close_socket(sock)

                    # if something really failed
            except Exception as e:
                addr = SOCKET_LIST[sock]
                print("Exception %r occurred while receiving a message from %s" % (e, addr))
                send_reply(addr, "Something failed in the server. Goodbye")
                close_socket(sock)
                continue

    SERVER_SOCKET.close()

    return 0

def close_socket(sock):
    global SOCKET_LIST
    print("Closing socket %s" % sock)

    # broken socket connection
    sock.close()
    # broken socket, remove it
    if sock in SOCKET_LIST:
        del SOCKET_LIST[sock]

def send_reply(sock, message):
    global SOCKET_LIST, SERVER_SOCKET
    if sock != SERVER_SOCKET:
        # print("send_reply: sending message '%s' to %s" % (message, SOCKET_LIST[sock]))
        # Have to convert the message to an array of bytes before sending them
        sock.send(bytes(message, 'UTF-8'))

if __name__ == "__main__":

    ret = chat_server()

    sys.exit(ret)
