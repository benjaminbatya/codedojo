#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# chat_client.py

# Adapted from http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
import sys
import time

from chat_client_funcs import ChatClient

HOST = '127.0.0.1'
PORT = 9000

LOCAL_PROMPT = '>> '

try:
    # Setup the ChatClient to talk to the server
    socket = ChatClient(HOST, PORT)

    print("Connected to remote host %s. Type in your message and press Enter to send it. The server's response will be displayed afterwards. Type 'quit' to quit." % socket.get_remote_addr())

    while True:

        # Print out all of the messages that the server has sent
        while socket.has_messages():
            msg = socket.get_message()
            print(msg)

        # wait for the user to type in a line
        msg = input(LOCAL_PROMPT).strip()

        # Quit the program if quit is typed in
        if msg.lower() == 'quit':
            break
        else:
            # Otherwise send the message
            socket.send_message(msg)

        # Wait for a little bit for the server to process
        time.sleep(0.3)

finally:
    # Always clean up at the end
    socket.disconnect()
