#!/usr/bin/python3


# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# This is an example chat client written in python
# It uses TCP sockets and turtle

import turtle
import turtle_funcs
from chat_client_funcs import ChatClient

# Setup the client socket
server_address = "127.0.0.1" # Change this to the correct address
port = 9000 # Change this to the correct port number
socket = ChatClient(server_address, port)

# Setup the graphics
WIN_WIDTH = 600
WIN_HEIGHT = 800
FG_COLOR = 'black'
FONT = ('Ariel', 16, 'normal')
NUM_MESSAGES = 20

screen = turtle_funcs.setup_graphics("DojoChat", WIN_WIDTH, WIN_HEIGHT)
screen.bgcolor('white')
# screen.bgpic('background.gif')  # Use a cool background picture instead of black

# make an input poller
key_poller = turtle_funcs.KeyPoller(screen)

# Variables to use
prev_messages = []
current_message = ''
do_redraw = True
is_running = True

def handle_socket():
    global prev_messages, do_redraw

    while(socket.has_messages()):
        do_redraw = True
        msg = socket.get_message()
        # print(msg)
        prev_messages.append(msg)

def handle_input():
    global is_running, do_redraw
    global key_poller, current_message, socket

    for keysym in key_poller.released():
        do_redraw = True
        key_ascii = turtle_funcs.get_ascii(keysym)

        if key_ascii:                 # Add the character to the message
            current_message += key_ascii
        elif keysym == 'Escape':       # Quit the app
            is_running = False
        elif keysym == 'Return':       # Send the message
            socket.send_message(current_message)
            current_message = ''
        elif keysym == 'BackSpace':    # Remove the last character
            current_message = current_message[:-1]

def draw_message(msg, x, y):
    turtle.goto(x, y)
    turtle.write(msg, font = FONT)

def redraw():
    global do_redraw
    if do_redraw:
        do_redraw = False

        turtle.clear()

         # Draw the current message
        x = -WIN_WIDTH/2 + FONT[1]/4
        y = -WIN_HEIGHT/2 + FONT[1]/2
        turtle.goto(x, y)
        out_str = current_message + '_'
        turtle.write(out_str, font = FONT)

        # Draw the line between current_message and prev messages
        y += FONT[1]*2
        turtle.pencolor(FG_COLOR)
        turtle.pensize(FONT[1]/6)
        turtle.goto(-WIN_WIDTH/2-10, y)
        turtle.pendown()
        turtle.goto(WIN_WIDTH/2+10, y)
        turtle.penup()
        turtle.home()

        # Draw the last NUM_MESSAGES messages
        y += FONT[1]/2
        N = len(prev_messages)
        for i in range(N-1, N-1-NUM_MESSAGES, -1):
            if i < 0:
                break
            msg = prev_messages[i]
            draw_message(msg, x, y)
            # figure out the number of lines in the message so we know how many lines to go up
            num_lines = msg.count('\n')+1
            y += FONT[1]*2*num_lines

        turtle.home()

    turtle.update()

try:
    # Run the event loop
    while(is_running):

        handle_socket()

        handle_input()

        redraw()

    turtle.bye()

except Exception as e: # Ignore any of Tkinter's thrown exceptions for now...
    print("Caught exception %r" % e)
    raise e

finally:
    print("Disconnecting from the server. Bye!")
    socket.disconnect()
