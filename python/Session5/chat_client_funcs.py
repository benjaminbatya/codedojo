
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# Simple client tcp ascii messaging socket
import socket
import sys
import threading
from collections import deque

class ChatClient(threading.Thread):
    def __init__(self, addr, port, verbose=False):
        super(ChatClient,self).__init__()
        self.addr = addr
        self.port = port
        self.verbose = verbose
        self._msg_queue = deque()

        self._do_run = True

        # Create and initialize the socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(0.1)
        try:
            self._socket.connect((self.addr, self.port))
        except:
            sys.stderr.write("Unable to connect to %s\n" % str(self.addr))
            self._do_run = False
            return
        else:
            self._print("Connected to remote server %s" % str(self.addr))

        self.start()

    def run(self):
        try:
            while self._do_run:
                # Handle the socket input
                msg = None
                # print("Checking %s" % str(self.socket.getpeername()))
                try:
                    msg = self._socket.recv(4096)
                except socket.error as e:
                    err = e.args[0]
                    # Ignore a time out error
                    if err != 'timed out':
                        raise
                if msg:
                    msg = msg.decode('UTF-8')
                    self._print("Received '%s' from '%s'" % (msg, str(self._socket.getpeername())))
                    self._msg_queue.append(msg)
                    # print self.prev_messages
        finally:
            self._socket.close()

    def _print(self, msg):
        if(self.verbose):
            print(msg)

    def has_messages(self):
        return True if self._msg_queue else False

    def get_message(self):
        if self.has_messages():
            return self._msg_queue.popleft()
        else:
            return None

    def send_message(self, msg):
        self._socket.send(bytes(msg, 'UTF-8'))

    def get_local_addr(self):
        return str(self._socket.getsockname()[0])

    def get_remote_addr(self):
        return str(self._socket.getpeername())

    def disconnect(self):
        # self._socket.sendall(bytes(chr(3), 'UTF-8'))
        self._do_run = False
        self.join()