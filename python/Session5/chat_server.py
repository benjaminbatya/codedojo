#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# From http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
 
import sys
import socket
import select
import time

HOST = '' 
SOCKET_LIST = {}
# Strong limit on the number of characters allowed to be received
RECV_BUFFER = 256
PORT = 9000

g_num_messages = 0

# This is a simple reverse echo server
def process_message(addr, message=""):
    message = message.lower()

    global g_num_messages
    g_num_messages += 1

    if message == "":
        reply = "Hello, I am an reverse echo server. I repeat back to you the reverse of anything that you say"
    elif message == "/time":
        reply = str(time.asctime())
    elif message == "/count":
        reply = "Num messages = %r" % g_num_messages
    elif message == "/users":
        users = [str(SOCKET_LIST[s]) for s in SOCKET_LIST]
        reply = "Users = '%s'" % ",".join(users)
    else:
        reply = message[::-1]

    return reply

def chat_server():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
 
    # add server socket object to the list of readable connections
    SOCKET_LIST[server_socket] = HOST + ':' + str(PORT)

    # figure out my IP address
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com", 80))
    host_name = s.getsockname()[0]
    s.close()
    print("Server started on IP address %s at port %d" % (host_name, PORT))
 
    while 1:

        # get the list sockets which are ready to be read through select
        # 4th arg, time_out  = 0 : poll and never block
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:

            # a new connection request received
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST[sockfd] = addr

                reply = process_message(addr)
                print("Client %r connected, reply = %s" % (addr, reply))
                send_reply(sockfd, reply)
             
            # a message from a client, not a new connection
            else:
                try:
                    # receiving data from the socket.
                    data = sock.recv(RECV_BUFFER)

                    # there is something in the socket
                    if data:
                        message = data.decode('UTF-8')

                        if(len(message) >= RECV_BUFFER):
                            # The client has tried to send too much data
                            send_reply(sock, "You are trying to send too much data.\nTry again to hack this server. Goodbye")
                            close_socket(sock)

                        else:
                            # process message received from client,
                            addr = SOCKET_LIST[sock]
                            reply = process_message(addr, message)
                            print("Received '%s' from %r, reply = %s" % (message, addr, reply))
                            send_reply(sock, reply)
                    else:
                        # at this stage, no data means probably the connection has been broken
                        close_socket(sock)

                # handle exception
                except Exception as e:
                    print("Exception %r occurred. Trying to recover" % e)
                    send_reply(sock, "Something failed in the server. Goodbye")
                    close_socket(sock)
                    continue

    server_socket.close()

    return 0

def close_socket(sock):
    print("Broken socket %s" % sock)

    # broken socket connection
    sock.close()
    # broken socket, remove it
    if sock in SOCKET_LIST:
        del SOCKET_LIST[sock]

def send_reply(sock, message):
    try :
        # print("Broadcasting %s to %s" % (message, str(socket.getpeername())))
        sock.send(bytes(message, 'UTF-8'))
    except :
        close_socket(sock)

if __name__ == "__main__":

    ret = chat_server()

    sys.exit(ret)
