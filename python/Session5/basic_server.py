#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally developed by Benjamin Schleimer <bensch one two eight at yahoo dot com>.
# Presented at CodeDojo Silicon Valley in May, 2016.

# Server setup code from
# http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
 
import sys
import socket
import select

# The server starts on localhost which is the empty string by default
HOST = ''
# 9000 is the Default port for the server
PORT = 9000

SOCKET_LIST = {}
SERVER_SOCKET = None

# Strong limit on the number of characters allowed to be received to prevent flood attacks
RECV_BUFFER = 256

def process_message(addr, message=""):
    '''
    This is a simple echo server
    :param addr: address of the client that sent the message
    :param message: Message that the client sent. if it's an empty string, then this is the initial connection reply
    :return: The reply to the client
    '''

    if message == "":
        reply = "Hello, I am a simple echo server. I repeat back to you anything that you type.\nYour address is %s" % addr
    else:
        # Otherwise, just echo the message back
        reply = message

    return reply

def chat_server():
    '''
    This runs the server in a forever loop. It gets connections from clients,
    opens sockets to them and then processes the replies in process_message()
    :return: None
    '''
    global SERVER_SOCKET, SOCKET_LIST
    # Setup the socket to use IPv4 and TCP (==STREAM)
    SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # This tells the socket to reuse the address
    SERVER_SOCKET.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Setup the socket to listen on the localhost and on PORT
    SERVER_SOCKET.bind((HOST, PORT))
    # Can listen to 50 connections at a time
    SERVER_SOCKET.listen(50)

    # figure out the server's IP address on the local network so clients can connect to it
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com", 80))
    host_name = s.getsockname()[0]
    s.close()
    server_address = host_name

    # add server socket object to the list of readable connections
    SOCKET_LIST[SERVER_SOCKET] = server_address

    print("Server started at %s and on port %d" % (server_address, PORT))

    while 1:

        # get the list sockets which are ready to be read through select
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST.keys(),[],[])
      
        for sock in ready_to_read:
            try:
                # a new connection request received
                if sock == SERVER_SOCKET:
                    # Accept the connection to bind the socket file descriptor to the address
                    # It will remain bound until the socket is closed
                    sockfd, full_addr = SERVER_SOCKET.accept()

                    print(full_addr)

                    # Only care about the IP address for now.
                    # We don't allow multiple clients from the same IP address
                    addr = full_addr[0]

                    # Try to prevent a sock flood attack by checking
                    # # if the address is already part of a sock in the socket list
                    # dup_sock = None
                    # for s in SOCKET_LIST:
                    #     if SOCKET_LIST[s].startswith(addr):
                    #         dup_sock = s
                    #         break
                    # if dup_sock:
                    #     close_socket(dup_sock)
                    #     sockfd.close()
                    #     continue

                    SOCKET_LIST[sockfd] = addr

                    reply = process_message(addr)
                    print("Client at %s connected" % addr)
                    send_reply(sockfd, reply)

                # a message from a client, not a new connection
                else:
                    # receiving data from the socket.
                    data = sock.recv(RECV_BUFFER)

                    # there is something in the socket
                    if data:
                        message = data.decode('UTF-8')

                        if(len(message) >= RECV_BUFFER):
                            # The client has tried to send too much data
                            send_reply(sock, "You are trying to send too much data.\nTry something else to hack this server. Goodbye")
                            raise RuntimeError("Hacking Attempt")

                        else:
                            # process the message from the client
                            addr = SOCKET_LIST[sock]
                            print("Received message '%s' from %s" % (message, addr))

                            reply = process_message(addr, message)
                            send_reply(sock, reply)
                    else:
                        # at this stage, no data means probably the connection has been broken so close the socket
                        close_socket(sock)

                    # if something really failed
            except Exception as e:
                addr = SOCKET_LIST[sock]
                print("Exception %r occurred while receiving a message from %s" % (e, addr))
                send_reply(addr, "Something failed in the server. Goodbye")
                close_socket(sock)
                continue

    SERVER_SOCKET.close()

    return 0

def close_socket(sock):
    global SOCKET_LIST
    print("Closing socket %s" % sock)

    # broken socket connection
    sock.close()
    # broken socket, remove it
    if sock in SOCKET_LIST:
        del SOCKET_LIST[sock]

def send_reply(sock, message):
    global SOCKET_LIST, SERVER_SOCKET
    if sock != SERVER_SOCKET:
        print("send_reply: sending message '%s' to %s" % (message, SOCKET_LIST[sock]))
        # Have to convert the message to an array of bytes before sending them
        sock.send(bytes(message, 'UTF-8'))

if __name__ == "__main__":

    ret = chat_server()

    sys.exit(ret)
