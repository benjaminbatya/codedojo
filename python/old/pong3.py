
import turtle
import _tkinter
# from tkinter import *
from client_socket import ClientSocket
import time
import math

class MyTurtle(turtle.Turtle):
    def __init__(self, shape):
        super().__init__(shape)
        # Don't have this draw
        self.up()

class MyScreen(object):
    def __init__(self):
        self.screen = turtle.Screen()
        self.canvas = self.screen.getcanvas()
        # self.canvas.
        #
        # self.screensize = turtle.Vec2D(*self.screen.screensize()) # * 0.5
        # print(self.screensize)

        self.o = MyTurtle("blocker")
        self.is_running = True

        self.count = 0
        self.is_drag = False
        self.pos = (0,0)

        # Listen for input events

    def _convert_event(self, event):
        pos = turtle.Vec2D(self.screen.cv.canvasx(event.x)/self.screen.xscale,
                          -self.screen.cv.canvasy(event.y)/self.screen.yscale)
        return pos

    def onpress(self, event):
        pos = self._convert_event(event)
        # (x,y) = (event.x, event.y)
        # print("onpress:")
        self.is_drag = True
        self.ondrag(event)

    def ondrag(self, event):
        if self.is_drag:
            pos = self._convert_event(event)
            # print("ondrag: pos=%r" % str(pos))
            self.pos = pos

    def onrelease(self, event):
        # print("onrelease: ")
        self.ondrag(event)
        self.is_drag = False

    def keypress(self, event):
        print("keypress = %r" % event.char)
        if event.char == '\x1b':
            self.is_running = False

    def run(self):
        self.screen.listen()
        self.canvas.bind("<KeyPress>", self.keypress)

        self.canvas.bind("<Button-1>", self.onpress)
        self.canvas.bind("<Button1-Motion>", self.ondrag)
        self.canvas.bind("<Button1-ButtonRelease>", self.onrelease)

        try:
            while(self.is_running):
                # turtle.clear()
                # write("Hello World!")
                # pos = self.count % 100 # math.sin(count / 100 * math.pi)
                self.o.setpos(self.pos)

                turtle.update()
                time.sleep(0.01)
                self.count += 1
        except _tkinter.TclError as e:
            pass

        # turtle.onkey()
        # turtle.onclick()

# class Screen(object):
#     def __init__(self, master):
#         self.master = master
#
#         self.win = Canvas(master, width=400, height=300)
#         self.win.pack()
#
#         self.win.after_idle(self.idle)
#
#         self.line = self.win.create_line(((50,50), (100,100)), fill="red")
#         self.count = 0
#
#
#
#     def idle(self):
#         print("idle")

def main():
    # reset()
    turtle.hideturtle()
    turtle.tracer(0,0)
    turtle.speed(0)
    turtle.Screen().bgcolor('black')

    turtle.title("Pong for CodeDojo")

    # Make the first blocker
    s = turtle.Shape("compound", None)
    poly = ((-50,-25), (50,-25), (50,25),(-50,25))
    s.addcomponent(poly, 'gray', 'white')
    turtle.getscreen().addshape("blocker", s)

    screen = MyScreen()
    screen.run()

    # master = Tk()
    #
    # s = Screen(master)
    #
    # mainloop()

if __name__ == '__main__':
    main()
    # turtle.mainloop()