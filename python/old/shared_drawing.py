#!/usr/bin/python

# This is an example chat client written in python
# It uses TCP sockets and pygame

import pygame
import pygame.locals

pygame.init()

from client_socket import ClientSocket

from widget import Widget
from my_signal import Signal

from button import Button
from base_app import BaseApp
from canvas import Canvas

FONT_SIZE = 20
FG_COLOR = (255, 255, 255)
INPUT_HEIGHT = FONT_SIZE + 5

# from http://stackoverflow.com/questions/10077644/python-display-text-w-font-color
FONT = pygame.font.SysFont("monospace", FONT_SIZE)

class SharedDrawing(BaseApp):
    BUTTON_SIZE = (120, 50)
    BUTTON_RECT = pygame.Rect((0,0), BUTTON_SIZE)

    def __init__(self, addr="127.0.0.1", serverport=9009):

        super(SharedDrawing, self).__init__()

        self.socket = ClientSocket(addr, serverport)

        self.prev_messages = []

    # START pygame related functionality

    def setup(self):

        super(SharedDrawing, self).setup("CodeDojo Shared Drawing App. Written in Python", 800, 480)

        self.bg_surface = pygame.image.load("../assets/game_background.png").convert()
        # self.image = pygame.image.load("game_sprite.png").convert_alpha()

        self.font = FONT

        draw_lines_rect = self.BUTTON_RECT.move(0, 10)

        self.draw_lines_button = Button("Draw Lines", draw_lines_rect, self.font)
        self.draw_lines_button.click_signal.connect(self.on_draw_lines)
        self.children.append(self.draw_lines_button)

        save_image_rect = self.BUTTON_RECT.move(0, 70)

        self.save_image_button = Button("Save Image", save_image_rect, self.font)
        self.save_image_button.click_signal.connect(self.on_save_image)
        self.children.append(self.save_image_button)

        self.canvas = Canvas(pygame.Rect(0, 0, 0, 0), self.font)
        self.children.append(self.canvas)

        self.on_resize()

        self.resize_signal.connect(self.on_resize)
        self.quit_signal.connect(self.on_quit)

    def on_draw_lines(self):
        print("on_draw_lines: called")

    def on_save_image(self):
        print("on_save_image: called")

    def on_quit(self):
        # Inform the server that this client is  disconnecting
        self.socket.disconnect()

    def on_resize(self):
        # print("on_resize called")

        screen_size = self.screen.get_size()
        self.draw_lines_button.rect.x = screen_size[0]-self.BUTTON_SIZE[0]
        self.save_image_button.rect.x = screen_size[0]-self.BUTTON_SIZE[0]
        self.canvas.rect.width = screen_size[0]-self.BUTTON_SIZE[0]
        self.canvas.rect.height = screen_size[1]

    def render(self):
        (width, height) = self.screen.get_size()

        # Draw the display
        pygame.transform.scale(self.bg_surface, (width, height), self.screen)

    def update(self):
        while self.socket.has_messages():
            self.prev_messages.append(self.socket.get_message())



if __name__ == "__main__":
    g = SharedDrawing()
    g.run()
