from turtle import *
import _tkinter
import time

class Object(Turtle):
    def __init__(self, shape):
        super().__init__(shape)
        # Don't have this turtle draw anything but its shape
        self.up()

# reset()
screen = Screen()
setup(800, 600, 0, 0)
hideturtle()
tracer(0,0)
speed(0)
screen.bgcolor('black')

title("Pong for CodeDojo")

# Make the first blocker
s = Shape("compound", None)
poly = ((-50,-25), (50,-25), (50,25),(-50,25))
s.addcomponent(poly, 'gray', 'white')
screen.addshape("blocker", s)

# Create a
blocker = Object("blocker")
blocker.setpos(-350, 0)

screen.listen()

def goup():
    pos = blocker.pos() + (0, 10)
    blocker.setpos(pos)

screen.onkey(goup, 'q')

def godown():
    pos = blocker.pos() + (0, -10)
    blocker.setpos(pos)

screen.onkey(godown, 'a')

is_running = True
def quit():
    global is_running
    is_running = False
# screen.onkey(quit, 'space')

try:
    while(is_running):
        update()
        # time.sleep(0.01)

except Exception as e: # Ignore any of the exceptions for now...
    print("Caught exception %r" % e)
    pass
